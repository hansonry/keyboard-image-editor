#ifndef __COLOR_H__
#define __COLOR_H__
#include <stdint.h>

struct colorRGB
{
   uint8_t r; // value between 0 and 255
   uint8_t g; // value between 0 and 255
   uint8_t b; // value between 0 and 255
   uint8_t a; // value between 0 and 255
};

struct colorHSV
{
   double h; // angle in degrees
   double s; // a fraction between 0 and 1
   double v; // a fraction between 0 and 1 
   double a;  // value between 0 and 255
};

void Color_PackedToRGB(struct colorRGB * rgb, unsigned int hex);

void Color_RGBToHSV(struct colorHSV * hsv, const struct colorRGB * rgb);
void Color_HSVToRGB(struct colorRGB * rgb, const struct colorHSV * hsv);


void Color_LerpHSV(struct colorHSV * dest, const struct colorHSV * a, 
                                           const struct colorHSV * b,
                                           double p);

#endif // __COLOR_H__
