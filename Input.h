#ifndef __INPUT_H__
#define __INPUT_H__
#include <stdbool.h>

enum kieInputEventType
{
   eKIET_None,
   eKIET_Paint,
   eKIET_CursorUp,
   eKIET_CursorUpLeft,
   eKIET_CursorLeft,
   eKIET_CursorDownLeft,
   eKIET_CursorDown,
   eKIET_CursorDownRight,
   eKIET_CursorRight,
   eKIET_CursorUpRight,
   eKIET_PalletUp,
   eKIET_PalletDown,
   eKIET_PalletLeft,
   eKIET_PalletRight,
   eKIET_EditorZoomIn,
   eKIET_EditorZoomOut,
   eKIET_PalletIndex0,
   eKIET_PalletIndex1,
   eKIET_PalletIndex2,
   eKIET_PalletIndex3,
   eKIET_PalletIndex4,
   eKIET_PalletIndex5,
   eKIET_PalletIndex6,
   eKIET_PalletIndex7,
   eKIET_PalletIndex8,
   eKIET_PalletIndex9,
};

struct kieInputEvent
{
   enum kieInputEventType type;
   bool fast;
   bool paintIsPressed;
};

void Input_GetCursorVector(enum kieInputEventType type, int * x, int * y);

#endif // __INPUT_H__
