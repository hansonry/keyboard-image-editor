#ifndef __COMMANDPARSER_H__
#define __COMMANDPARSER_H__


#define COMMANDPARSER_MAX_PARAMS 16

enum commandParseResult
{
   eCPR_EmptyCommand,
   eCPR_NotACommand,
   eCPR_CommandNotFound,
   eCPR_CommandFound
};

struct commandParserParamList
{
   const struct commandParserCommand * command;
   int commandIndex;
   int count;
   int forceFlag;
   const char * cmdTextStart;
   size_t cmdTextLength;
   const char * param[COMMANDPARSER_MAX_PARAMS];
   size_t length[COMMANDPARSER_MAX_PARAMS];
};


struct commandParserCommand
{
   const char * cmd;
   const char * longCmd;
   const char * paramSeperator; // NULL here indicates whitespace seperation
   int key;
};


void CommandParser_Init(const struct commandParserCommand * commands);
void CommandParser_Destroy(void);

enum commandParseResult CommandParser_Parse(const char * command, struct commandParserParamList * list);


#endif // __COMMANDPARSER_H__
