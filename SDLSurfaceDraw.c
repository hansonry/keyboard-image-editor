#include "SDLSurfaceDraw.h"

SDL_Surface * SDLSurfaceDraw_New(int width, int height)
{
   return SDL_CreateRGBSurface(0, width, height, 32, 0, 0, 0, 0);
}

static inline void SDLSurfaceDraw_InlineSet(SDL_Surface * surf, int x, int y,  Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
   Uint32 pixel = SDL_MapRGBA(surf->format, r, g, b, a);
   Uint32 *target_pixel = (Uint32*)(((Uint8 *)surf->pixels) + 
                                     y * surf->pitch +
                                     x * sizeof(*target_pixel));
   *target_pixel = pixel;
}

void SDLSurfaceDraw_SetRGB(SDL_Surface * surf, int x, int y, const struct colorRGB * color)
{
   SDLSurfaceDraw_InlineSet(surf, x, y, color->r, color->g, color->b, color->a);
}

void SDLSurfaceDraw_SetHSV(SDL_Surface * surf, int x, int y, const struct colorHSV * color)
{
   struct colorRGB rgb;
   Color_HSVToRGB(&rgb, color);
   SDLSurfaceDraw_InlineSet(surf, x, y, rgb.r, rgb.g, rgb.b, rgb.a);
}

void SDLSurfaceDraw_GetRGB(SDL_Surface * surf, int x, int y, struct colorRGB * color)
{
   Uint32 *target_pixel = (Uint32*)(((Uint8 *)surf->pixels) + 
                                     y * surf->pitch +
                                     x * sizeof(*target_pixel));
   SDL_GetRGBA(*target_pixel, surf->format, &color->r, 
                                            &color->g, 
                                            &color->b, 
                                            &color->a);
}
