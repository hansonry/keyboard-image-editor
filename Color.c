#include "Color.h"

void Color_PackedToRGB(struct colorRGB * rgb, unsigned int hex)
{
   rgb->r = (hex >> 24) & 0xFF;
   rgb->g = (hex >> 16) & 0xFF;
   rgb->b = (hex >>  8) & 0xFF;
   rgb->a = (hex >>  0) & 0xFF;
}

void Color_RGBToHSV(struct colorHSV * hsv, const struct colorRGB * rgb)
{
   double r, b ,g;
   double min, max, delta;
   
   hsv->a = rgb->a / 255.0; 
   
   // Conver to percents because that is the algrithm I am copying
   r = rgb->r / 255.0;
   g = rgb->g / 255.0;
   b = rgb->b / 255.0;
   
   min = r   < g ? r   : g;
   min = min < b ? min : b;

   max = r   > g ? r   : g;
   max = max > b ? max : b;

   hsv->v = max;
   delta = max - min;
   if(delta < 0.00001)
   {
      hsv->s = 0;
      hsv->h = 0;
      return;
   }
   
   if(max > 0.0)
   {
      hsv->s = (delta / max);
   }
   else
   {
      // if max is 0, then r = g = b = 0              
      // s = 0, h is undefined
      hsv->s = 0;
      hsv->h = 0;
      return ;
   }
   
   if(r >= max)
   {
      hsv->h = (g - b) / delta;
   }
   else if (g >= max)
   {
      hsv->h = 2.0 + (b - r) / delta;
   }
   else
   {
      hsv->h = 4.0 + (r - g) / delta;
   }
   
   hsv->h *= 60.0;
   if(hsv->h < 0.0)
   {
      hsv->h += 360.0;
   }
}

void Color_HSVToRGB(struct colorRGB * rgb, const struct colorHSV * hsv)
{
   double hh, ff;
   long i;
   uint8_t p, q, t;
   uint8_t u8v = (uint8_t)(hsv->v * 255);
   
   rgb->a = (uint8_t)(hsv->a * 255);
   
   if(hsv->s <= 0.0)
   {
      rgb->r = u8v;
      rgb->g = u8v;
      rgb->b = u8v;
      return;
   }
   hh = hsv->h;
   if(hh >= 360.0) hh = 0.0;
   hh /= 60.0;
   i = (long)hh;
   ff = hh - i;
   p = (uint8_t)((hsv->v * (1.0 - hsv->s)) * 255);
   q = (uint8_t)((hsv->v * (1.0 - (hsv->s * ff))) * 255);
   t = (uint8_t)((hsv->v * (1.0 - (hsv->s * (1.0 - ff)))) * 255);
   
   //printf("i: %d, u8v: %d, p: %d, q: %d, t: %d\n", i, u8v, p, q, t);
   switch(i)
   {
   case 0:
      rgb->r = u8v;
      rgb->g = t;
      rgb->b = p;
      break;
   case 1:
      rgb->r = q;
      rgb->g = u8v;
      rgb->b = p;
      break;
   case 2:
      rgb->r = p;
      rgb->g = u8v;
      rgb->b = t;
      break;
   case 3:
      rgb->r = p;
      rgb->g = q;
      rgb->b = u8v;
      break;
   case 4:
      rgb->r = t;
      rgb->g = p;
      rgb->b = u8v;
      break;
   case 5:
   default:
      rgb->r = u8v;
      rgb->g = p;
      rgb->b = q;
      break;
   }
}

void Color_LerpHSV(struct colorHSV * dest, const struct colorHSV * a, 
                                           const struct colorHSV * b,
                                           double p)
{
   double diff, targetHue;
   // H is a color wheel, so we have to find the best direction
   diff = b->h - a->h;
   if(diff > 180 || diff < -180)
   {
      if(b->h > a->h)
      {
         targetHue = a->h - 360;
      }
      else
      {
         targetHue = a->h + 360;
      }
   }
   else 
   {
      targetHue = a->h;
   }
   
   dest->h = (targetHue - a->h) * p + a->h;
   dest->s = (b->s - a->s)      * p + a->s;
   dest->v = (b->v - a->v)      * p + a->v;
   dest->a = (b->a - a->a)      * p + a->a;
   
   // normalize hue
   if(dest->h < 0)   dest->h += 360;
   if(dest->h > 360) dest->h -= 360;
   //printf("h: %f, s: %f, v: %f\n", new.h, new.s, new.v);   
}
