#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "SDL.h"
#include "SDL_ttf.h"
#include "TextTexture.h"

#define GROWBY 32

void TextTexture_Init(struct textTexture * tt, TTF_Font * font)
{
   tt->font                = font;
   tt->texture             = NULL;
   tt->textureWidth        = 0;
   tt->textureHeight       = 0;
   tt->textWidth           = 0;
   tt->textHeight          = 0;
   tt->text                = malloc(GROWBY);
   tt->textSize            = GROWBY;
   tt->color.r             = 255;
   tt->color.g             = 255;
   tt->color.b             = 255;
   tt->color.a             = SDL_ALPHA_OPAQUE;
   tt->textSizeIsOutOfDate = true;
   tt->textureIsOutOfDate  = true;
}

void TextTexture_Destroy(struct textTexture * tt)
{
   tt->font                = NULL;
   if(tt->texture != NULL)
   {
      SDL_DestroyTexture(tt->texture);
      tt->texture          = NULL;
   }
   tt->textureWidth        = 0;
   tt->textureHeight       = 0;
   tt->textWidth           = 0;
   tt->textHeight          = 0;
   free(tt->text);
   tt->text                = NULL;
   tt->textSize            = GROWBY;
   tt->textSizeIsOutOfDate = true;
   tt->textureIsOutOfDate  = true;
}

void TextTexture_MakeRoomFor(struct textTexture * tt, size_t size)
{
   if(tt->textSize < size)
   {
      tt->textSize = size + GROWBY;
      tt->text = realloc(tt->text, tt->textSize);
   }
}

void TextTexture_Write(struct textTexture * tt, const char * format, ...)
{
   int result;
   va_list args;
   va_start(args, format);
   result = vsnprintf(tt->text, tt->textSize, format, args); 
   va_end(args);
   
   if(result >= tt->textSize)
   {
      TextTexture_MakeRoomFor(tt, result + 1);
      va_start(args, format);
      (void)vsnprintf(tt->text, tt->textSize, format, args); 
      va_end(args);
   }
   tt->textureIsOutOfDate  = true;
   tt->textSizeIsOutOfDate = true;
}

void TextTexture_VWrite(struct textTexture * tt, const char * format, va_list args)
{
   va_list args2;
   int result;

   va_copy(args2, args);
   result = vsnprintf(tt->text, tt->textSize, format, args);
   if(result >= tt->textSize)
   {
      TextTexture_MakeRoomFor(tt, result + 1);
      (void)vsnprintf(tt->text, tt->textSize, format, args2);
   }
   va_end(args2);
   tt->textureIsOutOfDate  = true;
   tt->textSizeIsOutOfDate = true;
}

static void TextTexture_Generate(struct textTexture * tt, SDL_Renderer * rend)
{
   SDL_Surface * surf;
  
   if(tt->texture != NULL)
   {
      SDL_DestroyTexture(tt->texture);
   }
  
   surf = TTF_RenderText_Blended(tt->font, tt->text, tt->color);
  
   tt->texture = SDL_CreateTextureFromSurface(rend, surf);
   SDL_FreeSurface(surf);
   SDL_QueryTexture(tt->texture, NULL, NULL, &tt->textureWidth, &tt->textureHeight);
      
   tt->textureIsOutOfDate  = false;
}

static void TextTexture_UpdateSize(struct textTexture * tt)
{
   TTF_SizeText(tt->font, tt->text, &tt->textWidth, &tt->textHeight);
   tt->textSizeIsOutOfDate = false;
}

void TextTexture_GetSize(struct textTexture * tt, int * width, int * height)
{
   if(tt->textSizeIsOutOfDate)
   {
      TextTexture_UpdateSize(tt);
   }
   
   if(width != NULL)
   {
      (*width) = tt->textWidth;
   }
   if(height != NULL)
   {
      (*height) = tt->textHeight;
   }
}

SDL_Texture * TextTexture_GetTexture(struct textTexture * tt, SDL_Renderer * rend, int * width, int * height)
{
   if(tt->textureIsOutOfDate)
   {
      TextTexture_Generate(tt, rend);
   }
   if(width != NULL)
   {
      (*width) = tt->textureWidth;
   }
   if(height != NULL)
   {
      (*height) = tt->textureHeight;
   }
   return tt->texture;
}

void TextTexture_Render(struct textTexture * tt, SDL_Renderer * rend, int x, int y)
{
   SDL_Texture * texture;
   SDL_Rect r;
   r.x = x;
   r.y = y;
   texture = TextTexture_GetTexture(tt, rend, &r.w, &r.h);
   SDL_RenderCopy(rend, texture, NULL, &r);
}
