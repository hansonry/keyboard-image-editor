# Keyboard Image Editor
An open source lightweight keyboard only image editor.

License: [GPL2](LICENSE)

## Missing Important features

The following features are missing, but planned. If you need these features,
then this software is not ready for you yet.

* Set whole image to color (clear)
* Drawing Lines
* Mirror editing
* Undo and Redo


## Screen Shots
![Screen shot of vent](doc/VentScreenShot.png)

## Controls

Uses the number pad to move the curosr around. You can hold shift to
move a larger distance.

Press `space` to place selected color. If you are holding space when 
your cursor moves, you automaticaly place the selected color in the
new cursor location.

Press `a` or `d` to select a diffrent color. Use `w` use a brighter form
of the the color. Use `s` to use a darker form of the color.

Number keys to jump to a color.

The `+` and `-` keys change the zoom by powers of 2.

The `:` will give you access to the console

### Console Commands

Hit `:` to enter the console. `ESC` will cancel your current command
and return you to editing. `Enter` or `Return` will execute your command if 
it is valid.

Some commands can be forced by putting an `!` after the command. For example
`:e smile.png` will load smile.png only if there are no unsaved changes to the 
current image. Where as `:e! smile.png` will load the current even if there
are unsaved changes. This will cause you to lose your work.

Pressing the up and down arrow key cycles the command history.

All file paths are realitive to the current working directory.

**File Commands**

* `:q`                  - Quits out of the program only if there are no unsaved
                          changes. Use the Force option `:q!` to quit if
                          you do have unsaved changes.
* `:w <filename>`       - Saves your current image as the file specified. If you 
                          have loaded or saved your image previously, then the 
                          filename is not required.
* `:e <filename>`       - Loads (Edits) the specified file only if there are no
                          unsaved changes. Use the Force option if you want 
                          load anyway. 
* `:n <width> <height>` - Creates a new image if there are no unsaved changes.
                          Use the force option to if you want to create a new
                          image without saving changes. Default values for 
                          width and height are 32. 

**Editor Settings**

* `:preview <scale> <x-tile> <y-tile>` - Configures the preview window. x-tile
                                         and y-tile are optional.
* `:preview-tile <x-tile> <y-tile>`    - Configures how many tile copies
                                         are shown in the preview window.

**Cursor Control**

* `:m <x-offset> <y-offset> <pallet-offset>` - Move the cursor relative to the 
                                               current position and optionally
                                               the active pallet index.
* `:M <x> <y> <pallet>`                      - Move the curosr to x and y. 
                                               Optionally you can set the 
                                               active pallet index.

**Color Control**

* `:pc <hex-or-red> <green> <blue> <alpha>` - Reads or sets the color of the 
                                              currently selected color. If 
                                              you don't provide any 
                                              parameters it will read back
                                              the current color (not tone
                                              shifted). If only the hex-or-red
                                              parameter is provided, then 
                                              the color will be set by the
                                              4 byte color value 0xRRBBGGAA
                                              where RR is the red value, BB 
                                              is the blue value, GG is the 
                                              green value, and AA is the alpha
                                              value. Alpha of 0xFF is fully
                                              opaque. Otherwise it will be 
                                              assumed the hex-or-red is the 
                                              red component value from 0 to 
                                              255. The green parameter range
                                              is 0 to 255. The read parameter
                                              is 0 to 255. The alpha parameter
                                              range is 0 to 255 and defaults 
                                              to opaque (255) if not specified.

