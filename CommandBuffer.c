#include <string.h>
#include <stdbool.h>
#include "SDL.h"
#include "SDL_ttf.h"
#include "CommandBuffer.h"

struct charBuffer
{
   char * base;
   size_t size;
   size_t count;
};

#define CHARBUFFER_GROWBY 32

static void CharBuffer_Init(struct charBuffer * buf, size_t size)
{
   if(size == 0)
   {
      buf->size = CHARBUFFER_GROWBY;
   }
   else
   {
      buf->size = size;
   }
   
   buf->count = 0;
   buf->base = malloc(buf->size);
}

static void CharBuffer_MakeRoomFor(struct charBuffer * buf, size_t size)
{
   if(buf->size < size)
   {
      buf->size = size + CHARBUFFER_GROWBY;
      buf->base = realloc(buf->base, buf->size);
   }
}

static void CharBuffer_Destroy(struct charBuffer * buf)
{
   free(buf->base);
   buf->count = 0;
   buf->size = 0;
   buf->base = NULL;
}

static int CharBuffer_SwapChar(struct charBuffer * to, 
                               struct charBuffer * from)
{
   if(from->count == 0)
   {
      return 0;
   }
   
   CharBuffer_MakeRoomFor(to, to->count + 1);
   
   to->base[to->count] = from->base[from->count - 1];
   to->count ++;
   from->count --;
   return 1;
}

#define HISTORY_GROWBY 32
struct commandHistory
{
   char * text;
};


static struct commandHistory * History;
static size_t HistorySize;
static size_t HistoryCount;
static size_t HistoryIndex;
static char * ActiveText;

static struct charBuffer Before;
static struct charBuffer After;
static struct charBuffer Full;
static bool IsFullDirty = true;

// SDL Specific Stuff
static bool IsTextureDirty = true;
static TTF_Font * Font = NULL;
static SDL_Texture * Texture = NULL;
static int TextureWidth = 0;
static int TextureHeight = 0;
static SDL_Color TextureColor;
static bool TextInputActive = false;
static struct charBuffer CursorPosBuffer;
static int CursorOffset;
static bool CursorPosChange = true;


void CommandBuffer_Init(TTF_Font * font)
{
   CharBuffer_Init(&Before, 0);
   CharBuffer_Init(&After,  0);
   CharBuffer_Init(&Full,   0);
   Font = font;
   IsFullDirty = true;
   IsTextureDirty = true;
   TextInputActive = false;
   Texture = NULL;
   TextureHeight = 0;
   TextureWidth  = 0;
   TextureColor.r = 255;
   TextureColor.g = 255;
   TextureColor.b = 255;
   TextureColor.a = SDL_ALPHA_OPAQUE;
   CharBuffer_Init(&CursorPosBuffer, 0);
   CursorPosChange = true;
   CursorOffset = 0;
   
   HistorySize = HISTORY_GROWBY;
   HistoryCount = 0;
   History = malloc(sizeof(struct commandHistory) * HistorySize);
   ActiveText = NULL;
   HistoryIndex = HistoryCount;
}

void CommandBuffer_Destroy(void)
{
   size_t i;
   CharBuffer_Destroy(&Before);
   CharBuffer_Destroy(&After);
   CharBuffer_Destroy(&Full);
   CharBuffer_Destroy(&CursorPosBuffer);
   if(Texture != NULL)
   {
      SDL_DestroyTexture(Texture);
   }
   for(i = 0; i < HistoryCount; i++)
   {
      free(History[i].text);
   }
   free(History);
   History      = NULL;
   HistoryCount = 0;
   HistorySize  = 0;
   
   if(ActiveText != NULL)
   {
      free(ActiveText);
      ActiveText = NULL;
   }
}


void CommandBuffer_SetText(const char * text)
{
   size_t len = strlen(text);
   CharBuffer_MakeRoomFor(&Before, len);
   After.count = 0;
   memcpy(Before.base, text, len);
   Before.count = len;
   IsFullDirty = true;
   IsTextureDirty = true;
   CursorPosChange = true;
   //printf("Set Text: %s\n", text);
}

void CommandBuffer_AddToHistroy(void)
{
   const char * text = CommandBuffer_GetText();
   if(HistoryCount == 0 || strcmp(text, History[HistoryCount - 1].text) != 0)
   {
      if(HistoryCount >= HistorySize)
      {
         HistorySize += HISTORY_GROWBY;
         History = realloc(History, sizeof(struct commandHistory) * HistorySize);
      }
      History[HistoryCount].text = strdup(text);
      HistoryCount ++;
   }
}

int CommandBuffer_MoveCursorLeft(void)
{
   int success = CharBuffer_SwapChar(&After, &Before);
   if(success) CursorPosChange = true;
   return success;
}


int CommandBuffer_MoveCursorRight(void)
{
   int success = CharBuffer_SwapChar(&Before, &After);
   if(success) CursorPosChange = true;
   return success;
}


void CommandBuffer_SetCursorPos(size_t pos)
{
   while(Before.count != pos)
   {
      if(Before.count > pos)
      {
         (void)CommandBuffer_MoveCursorRight();
      }
      else
      {
         (void)CommandBuffer_MoveCursorLeft();
      }
      CursorPosChange = true;
   }
}

void CommandBuffer_SetCursorPosToEnd(void)
{
   while(CommandBuffer_MoveCursorRight());
   CursorPosChange = true;
}

void CommandBuffer_AddTextAtCursor(const char * text)
{
   size_t len = strlen(text);
   CharBuffer_MakeRoomFor(&Before, Before.count + len);
   memcpy(Before.base + Before.count, text, len);
   Before.count += len;
   IsFullDirty = true;
   IsTextureDirty = true;
   CursorPosChange = true;
   //printf("Add Text: %s\n", text);
}

int CommandBuffer_Backspace(void)
{
   if(Before.count == 0)
   {
      return 0;
   }
   Before.count --;
   IsFullDirty = true;
   IsTextureDirty = true;
   CursorPosChange = true;
   return 1;
}

int CommandBuffer_Delete(void)
{
   if(After.count == 0)
   {
      return 0;
   }
   After.count --;
   IsFullDirty = true;
   IsTextureDirty = true;
   return 1;
}

size_t CommandBuffer_GetCount(void)
{
   return Before.count + After.count;
}

static void CommandBuffer_GenerateText(void)
{
   size_t i;
   size_t size = Before.count + After.count;
   CharBuffer_MakeRoomFor(&Full, size + 1);
   memcpy(Full.base, Before.base, Before.count);
   for(i = 0; i < After.count; i++)
   {
      size_t ri = After.count - i - 1;
      Full.base[Before.count + i] = After.base[ri];
   }
   Full.base[size] = '\0';
}

const char * CommandBuffer_GetText(void)
{
   if(IsFullDirty)
   {
      IsFullDirty = false;
      CommandBuffer_GenerateText();
   }
   return Full.base;
}

size_t CommandBuffer_GetCursorPos(void)
{
   return Before.count;
}

// SDL Specific Stuff

void CommandBuffer_SetFont(TTF_Font * font)
{
   Font = font;
   IsTextureDirty = true;
}

void CommandBuffer_SetColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a)
{
   TextureColor.r = r;
   TextureColor.g = g;
   TextureColor.b = b;
   TextureColor.a = a;
   IsTextureDirty = true;
}

static void CommandBuffer_GenerateTexture(SDL_Renderer * rend)
{
   SDL_Surface * surf;
   const char * text;
  
   if(Texture != NULL)
   {
      SDL_DestroyTexture(Texture);
   }
  
   text = CommandBuffer_GetText();
   surf = TTF_RenderText_Blended(Font, text, TextureColor);
   
   Texture = SDL_CreateTextureFromSurface(rend, surf);
   SDL_FreeSurface(surf);
   SDL_QueryTexture(Texture, NULL, NULL, &TextureWidth, &TextureHeight);
      
}

SDL_Texture * CommandBuffer_GetTexture(SDL_Renderer * rend, int * width, int * height)
{
   if(IsTextureDirty)
   {
      IsTextureDirty = false;
      CommandBuffer_GenerateTexture(rend);
   }
   if(width != NULL)
   {
      (*width) = TextureWidth;
   }
   if(height != NULL)
   {
      (*height) = TextureHeight;
   }
   return Texture;
}

void CommandBuffer_StartTextInput(void)
{
   TextInputActive = true;
   HistoryIndex = HistoryCount;
   //SDL_StartTextInput();
}

void CommandBuffer_StopTextInput(void)
{
   TextInputActive = false;
   //SDL_StopTextInput();
}

int CommandBuffer_IsTextInputActive(void)
{
   return TextInputActive;
}

void CommandBuffer_HandelSDLEvent(SDL_Event * event)
{
   if(TextInputActive)
   {
      if(event->type == SDL_KEYDOWN)
      {
         switch(event->key.keysym.sym)
         {
         case SDLK_BACKSPACE:
            CommandBuffer_Backspace();
            break;
         case SDLK_DELETE:
            CommandBuffer_Delete();
            break;
         case SDLK_LEFT:
            CommandBuffer_MoveCursorLeft();
            break;
         case SDLK_RIGHT:
            CommandBuffer_MoveCursorRight();
            break;
         case SDLK_UP:
            if(HistoryIndex >= HistoryCount)
            {
               if(ActiveText != NULL)
               {
                  free(ActiveText);
               }
               ActiveText = strdup(CommandBuffer_GetText());
            }
            if(HistoryIndex > 0)
            {
               HistoryIndex --;
            }
            if(HistoryIndex < HistoryCount)
            {
               CommandBuffer_SetText(History[HistoryIndex].text);
            }
            break;
         case SDLK_DOWN:
            if(HistoryIndex < HistoryCount)
            {
               HistoryIndex++;
               if(HistoryIndex < HistoryCount)
               {
                  CommandBuffer_SetText(History[HistoryIndex].text);
               }
               else
               {
                  CommandBuffer_SetText(ActiveText);
               }

            }
            break;
         }
      }
      else if(event->type == SDL_TEXTINPUT)
      {
         CommandBuffer_AddTextAtCursor(event->text.text);
      }
   }
}

int CommandBuffer_GetOffsetOfCharacter(size_t index)
{
   int offset;
   const char * text = CommandBuffer_GetText();
   if(index == 0)
   {
      offset = 0;
   }
   else if(index >= CommandBuffer_GetCount())
   {
      TTF_SizeText(Font, text, &offset, NULL);
   }
   else
   {
      // Bad SDL font API! I have to make my own shortend string :(
      CharBuffer_MakeRoomFor(&CursorPosBuffer, index + 1);
      memcpy(CursorPosBuffer.base, text, index);
      CursorPosBuffer.base[index] = '\0';
      TTF_SizeText(Font, CursorPosBuffer.base, &offset, NULL);
   }
   return offset;
}

void CommandBuffer_GetSizeOfText(int * width, int * height)
{
   const char * text = CommandBuffer_GetText();
   TTF_SizeText(Font, text, width, height);
}

int CommandBuffer_GetOffsetOfCursor()
{
   if(CursorPosChange)
   {
      CursorPosChange = false;
      CursorOffset = CommandBuffer_GetOffsetOfCharacter(CommandBuffer_GetCursorPos());
   }
   return CursorOffset;
}

void CommandBuffer_RenderText(SDL_Renderer * rend, int x, int y)
{
   SDL_Texture * texture;
   SDL_Rect dest;
   int cursorOffset = CommandBuffer_GetOffsetOfCursor();
   
   dest.x = x;
   dest.y = y;
   
   texture = CommandBuffer_GetTexture(rend, &dest.w, &dest.h);
   SDL_RenderCopy(rend, texture, NULL, &dest);
   
   SDL_SetRenderDrawColor(rend, TextureColor.r, TextureColor.g, 
                                TextureColor.b, TextureColor.a); 
   SDL_RenderDrawLine(rend, x + cursorOffset, y, x + cursorOffset, y + dest.h);
}

void CommandBuffer_RenderTextAtHeight(SDL_Renderer * rend, int x, int y, int height)
{
   SDL_Texture * texture;
   SDL_Rect dest;
   int cursorOffset = CommandBuffer_GetOffsetOfCursor();
   
   dest.x = x;
   dest.y = y;
   
   texture = CommandBuffer_GetTexture(rend, &dest.w, NULL);
   dest.h = height;
   SDL_RenderCopy(rend, texture, NULL, &dest);
   
   SDL_SetRenderDrawColor(rend, TextureColor.r, TextureColor.g, 
                                TextureColor.b, TextureColor.a); 
   SDL_RenderDrawLine(rend, x + cursorOffset, y, x + cursorOffset, y + dest.h);
}

