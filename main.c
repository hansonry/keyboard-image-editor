/*
 * A Keyboad Based Image Editor.
 * Copyright (C) 2020  Ryan Hanson
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */
#include "SDL.h"
#include "SDL_ttf.h"
#include "SDL_image.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include "CommandBuffer.h"
#include "TextTexture.h"
#include "CommandParser.h"
#include "Color.h"
#include "UI.h"
#include "UIPickerHSV.h"
#include "SDLSurfaceDraw.h"
#include "Input.h"

#define WINDOW_TITLE             "Keyboard Image Editor"
#define UI_COLOR_FRAME           128, 128, 128, SDL_ALPHA_OPAQUE
#define UI_COLOR_BACKGROUND      0,   0,   32,  SDL_ALPHA_OPAQUE
#define UI_COLOR_TEXTBACKGROUND  0,   0,   0,   SDL_ALPHA_OPAQUE
#define UI_COLORBOX_SIZE              16
#define PALLET_COLUMNS_SIZE           10

struct paletColumn
{
   struct colorRGB * list;
   int index;

   // Animation Purposes
   double offset;
};

static SDL_Window * Window;
static SDL_Surface * image;
static SDL_Texture * imageTexture = NULL;
static int CursorX = 0;
static int CursorY = 0;
static bool CursorWrap = true;
static int imageWidth  = 32;
static int imageHeight = 32;
static int scale = 8;
static int PreviewScale = 2;
static int PreviewTileX = 1;
static int PreviewTileY = 1;
static struct paletColumn PalletColumns[PALLET_COLUMNS_SIZE];
static int PalletColumnsSize = 13;
static int PalletX = 0;
static int PalletY = 0;
static bool PalletWrapX = true;
static bool PalletWrapY = true;
static bool PalletLoadPreviousRow = true;
static bool Running;

static int ScreenWidth  = 640;
static int ScreenHeight = 480;


static char * Filename = NULL;
static bool UnsavedChanges = false;

static struct textTexture TTCursorPosition;

static struct textTexture TTStatus;
static bool StatusVisible = false;

static bool DebugRenderUIBoxes = false;
static struct uiLayoutComponent UILayoutEditor;
static struct uiLayoutComponent UILayoutPreview;
static struct uiLayoutComponent UILayoutPallet;
static struct uiLayoutComponent UILayoutStatus;
static struct uiLayoutComponent UILayoutCoord;
static struct uiLayoutComponent UILayoutColorPicker;


static bool PalletInputEnabled = true;
static bool EditorInputEnabled = true;
static bool UIPickerHSVEnabled = false;

static inline void KIEUILayout_InitComponent(struct uiLayoutComponent * comp, 
                                             int desiredWidth, int desiredHeight, 
                                             int minWidth, int minHeight)
{
   UILayout_InitComponent(comp);
   comp->padding.left   = 3;
   comp->padding.top    = 3;
   comp->padding.right  = 3;
   comp->padding.bottom = 3;
   UILayout_SetSize(comp, desiredWidth, desiredHeight, minWidth, minHeight);
}

static inline void UILayout_CompToSDLRect(SDL_Rect * rect, const struct uiLayoutComponent * comp)
{
   rect->x = comp->component.computedOffset.x;
   rect->y = comp->component.computedOffset.y;
   rect->w = comp->component.computedDim.width;
   rect->h = comp->component.computedDim.height;
}

static void UIUpdateLayout(void)
{
   int cursorTextWidth, cursorTextHeight;

   struct uiLayoutComponent UILayoutRoot;
   struct uiLayoutComponent UILayoutEditorAndPreview;
   struct uiLayoutComponent UILayoutBottom;
   struct uiLayoutComponent UILayoutVerticalSpacer;
   struct uiLayoutComponent UILayoutPalletAndColor;
   
   // Get the text width and height of the cursor position
   TextTexture_GetSize(&TTCursorPosition, &cursorTextWidth, &cursorTextHeight);
   
   UILayout_InitVSplit(&UILayoutRoot);
   UILayout_InitHSplit(&UILayoutEditorAndPreview);
   UILayout_InitHSplit(&UILayoutBottom);
   UILayout_InitHSplit(&UILayoutPalletAndColor);

   UILayout_InitComponent(&UILayoutVerticalSpacer);
   UILayoutVerticalSpacer.expandY = 1;



   KIEUILayout_InitComponent(&UILayoutEditor, 
                             imageWidth  * scale,
                             imageHeight * scale,
                             3 * scale,
                             3 * scale);

   KIEUILayout_InitComponent(&UILayoutPreview, 
                             imageWidth  * PreviewScale * PreviewTileX,
                             imageHeight * PreviewScale * PreviewTileY,
                             0,
                             0);

   KIEUILayout_InitComponent(&UILayoutPallet, 
                             PALLET_COLUMNS_SIZE * UI_COLORBOX_SIZE,
                             PalletColumnsSize   * UI_COLORBOX_SIZE,
                             0,
                             0);
   
   KIEUILayout_InitComponent(&UILayoutStatus, 
                             0,
                             cursorTextHeight,
                             0,
                             cursorTextHeight);

   KIEUILayout_InitComponent(&UILayoutCoord, 
                             cursorTextWidth,
                             cursorTextHeight,
                             0,
                             cursorTextHeight);
   
   KIEUILayout_InitComponent(&UILayoutColorPicker, 
                             0,
                             0,
                             0,
                             0);

   UIPickerHSV_SetLayout(&UILayoutColorPicker);

   UILayoutStatus.expandX = 1;

   UILayoutBottom.anchor = eUIA_BottomLeft;
   UILayoutStatus.anchor = eUIA_BottomLeft;
   UILayoutCoord.anchor  = eUIA_BottomRight;
   
   UILayout_AddChild(&UILayoutRoot, &UILayoutEditorAndPreview);
   UILayout_AddChild(&UILayoutRoot, &UILayoutPalletAndColor);
   UILayout_AddChild(&UILayoutRoot, &UILayoutVerticalSpacer);
   UILayout_AddChild(&UILayoutRoot, &UILayoutBottom);
   
   UILayout_AddChild(&UILayoutEditorAndPreview, &UILayoutEditor);
   UILayout_AddChild(&UILayoutEditorAndPreview, &UILayoutPreview);
   
   UILayout_AddChild(&UILayoutPalletAndColor, &UILayoutPallet);
   if(UIPickerHSVEnabled)
   {
      UILayout_AddChild(&UILayoutPalletAndColor, &UILayoutColorPicker);
   }

   UILayout_AddChild(&UILayoutBottom, &UILayoutStatus);
   UILayout_AddChild(&UILayoutBottom, &UILayoutCoord);
   
   UILayout_SizeAndLayout(&UILayoutRoot, 0, 0, ScreenWidth, ScreenHeight, NULL);

}

static void StatusShow(const char * format, ...)
{
   va_list args;
   va_start(args, format);
   TextTexture_VWrite(&TTStatus, format, args);
   va_end(args);
   StatusVisible = true;
}

static void StatusHide(void)
{
   StatusVisible = false;
}

static void PalletUpdateColorPickerIfNessary(void);

static void StateSetEdit(void)
{
   PalletInputEnabled = true;
   EditorInputEnabled = true;
   UIPickerHSVEnabled = false;
}

static void StateSetHSVPicker(void)
{
   PalletInputEnabled = true;
   EditorInputEnabled = false;
   UIPickerHSVEnabled = true;
   PalletUpdateColorPickerIfNessary();
}

static void PalletSetColumnsSize(int newSize)
{
   int i, k;
   if(newSize != PalletColumnsSize)
   {
      for(i = 0; i < PALLET_COLUMNS_SIZE; i++)
      {
         PalletColumns[i].list = realloc(PalletColumns[i].list, 
                                         sizeof(struct colorRGB) * newSize);
         
         // Extend the last color to fill the new slots
         // Maybe this isn't the best way to handle this
         if(newSize > PalletColumnsSize)
         {
            for(k = PalletColumnsSize; k < newSize; k++)
            {
               memcpy(&PalletColumns[i].list[k],
                      &PalletColumns[i].list[PalletColumnsSize - 1],
                      sizeof(struct colorRGB));
            }
         }
         else
         {
            if(PalletY >= newSize)
            {
               PalletY = newSize - 1;
            }
            if(PalletColumns[i].index >= newSize)
            {
               PalletColumns[i].index = newSize - 1;
            }
         }
      }
      PalletColumnsSize = newSize;
      UIUpdateLayout();
   }
}

static struct colorRGB * PalletGetColorAt(int x, int y)
{
   return &PalletColumns[x].list[y];
}

static struct colorRGB * PalletGetSelectedColor(void)
{
   return &PalletColumns[PalletX].list[PalletY];
}

static void PalletUpdateColorPickerIfNessary(void)
{
   if(UIPickerHSVEnabled)
   {
      struct colorRGB * color = PalletGetSelectedColor();
      struct colorHSV hsv;
      Color_RGBToHSV(&hsv, color);
      UIPickerHSV_SetHSV(&hsv);
   }
}

static void PalletMoveLeft(void)
{
   PalletColumns[PalletX].index = PalletY;
   if(PalletX > 0)
   {
      PalletX --;
   }
   else if(PalletWrapX)
   {
      PalletX = PALLET_COLUMNS_SIZE - 1;
   }
   if(PalletLoadPreviousRow)
   {
      PalletY = PalletColumns[PalletX].index;
   }
   PalletUpdateColorPickerIfNessary();
}

static void PalletMoveRight(void)
{
   PalletColumns[PalletX].index = PalletY;
   if(PalletX < PALLET_COLUMNS_SIZE - 1)
   {
      PalletX ++;
   }
   else if(PalletWrapX)
   {
      PalletX = 0;
   }
   if(PalletLoadPreviousRow)
   {
      PalletY = PalletColumns[PalletX].index;
   }
   PalletUpdateColorPickerIfNessary();

}

static void PalletMoveUp(void)
{
   if(PalletY > 0)
   {
      PalletY --;
   }
   else if(PalletWrapY)
   {
      PalletY = PalletColumnsSize - 1;
   }
   PalletColumns[PalletX].offset -= 1.0;
   PalletUpdateColorPickerIfNessary();
}

static void PalletMoveDown(void)
{
   if(PalletY < PalletColumnsSize - 1)
   {
      PalletY ++;
   }
   else if(PalletWrapY)
   {
      PalletY = 0;
   }
   PalletColumns[PalletX].offset += 1.0;
   PalletUpdateColorPickerIfNessary();
}

static void PalletSetColumn(int col)
{
   if(col < 0)
   {
      col = 0;
   }
   if(col >= PALLET_COLUMNS_SIZE)
   {
      col = PALLET_COLUMNS_SIZE - 1;
   }

   if(col != PalletX)
   {
      PalletColumns[PalletX].index = PalletY;
      PalletX = col;
      if(PalletLoadPreviousRow)
      {
         PalletY = PalletColumns[PalletX].index;
      }
      PalletUpdateColorPickerIfNessary();
   }
}

static void PalletSetPos(int x, int y)
{
   if(x < 0)
   {
      x = 0;
   }
   if(x >= PALLET_COLUMNS_SIZE)
   {
      x = PALLET_COLUMNS_SIZE - 1;
   }
   if(y < 0)
   {
      y = 0;
   }
   if(y >= PalletColumnsSize)
   {
      y = PalletColumnsSize - 1;
   }

   if(x != PalletX)
   {
      PalletColumns[PalletX].index = PalletY;
      PalletX = x;
   }

   if(y != PalletY)
   {
      PalletY = y;
   }
   PalletUpdateColorPickerIfNessary();
}

static void PalletInit(int initSize)
{
   int i, k;
   PalletColumnsSize = initSize;
   for(i = 0; i < PALLET_COLUMNS_SIZE; i++)
   {
      PalletColumns[i].index = 0;
      PalletColumns[i].list = malloc(sizeof(struct colorRGB) * initSize);
      PalletColumns[i].offset = 0;
      for(k = 0; k < initSize; k ++)
      {
         Color_PackedToRGB(&PalletColumns[i].list[k], 0x00000000);
      }
   }
}

static void PalletCleanup(void)
{
   int i;
   for(i = 0; i < PALLET_COLUMNS_SIZE; i++)
   {
      free(PalletColumns[i].list);
      PalletColumns[i].list = NULL;
      PalletColumns[i].index = 0;
   }
   PalletColumnsSize = 0;
}

void set_pallet_to_defaults()
{
   int i, k;
   const int middleIndex = PalletColumnsSize / 2;
   struct colorHSV darkBlue, brightYellow;
   struct colorHSV hsv, interpolatedHSV;
   
   darkBlue.h = 242;
   darkBlue.s = 0.53;
   darkBlue.v = 0.05;
   darkBlue.a = 1;
   
   brightYellow.h = 59;
   brightYellow.s = 0.15;
   brightYellow.v = 0.99;
   brightYellow.a = 1;
   
   // Taken from https://lospec.com/palette-list/gemini10
   // Thanks Sabaku
   Uint32 colorList[PALLET_COLUMNS_SIZE] = {
      0x4c3052FF,
      0x778aa6FF,
      0xffffffFF,
      0xd4c957FF,
      0x3e9c3eFF,
      0x1f585cFF,
      0x141a54FF,
      0x000000FF,
      0x660000FF,
      0x9c5106FF
   };
   
   for(i = 0; i < PALLET_COLUMNS_SIZE; i++)
   {
      int start;
      struct colorRGB * colorRGB = &PalletColumns[i].list[middleIndex];
      Color_PackedToRGB(colorRGB, colorList[i]);
      Color_RGBToHSV(&hsv, colorRGB);
      for(k = 0; k < middleIndex; k++)
      {
         double p = k / (double)middleIndex;
         Color_LerpHSV(&interpolatedHSV, &darkBlue, &hsv, p);
         Color_HSVToRGB(&PalletColumns[i].list[k], &interpolatedHSV);
      }
      start = middleIndex + 1;
      for(k = start; k < PalletColumnsSize; k++)
      {
         double p = (k - start + 1) / (double)(PalletColumnsSize - start);
         Color_LerpHSV(&interpolatedHSV, &hsv, &brightYellow,  p);
         Color_HSVToRGB(&PalletColumns[i].list[k], &interpolatedHSV);
      }
      PalletColumns[i].index = middleIndex;
   }

   PalletY = middleIndex;
   
}

static void UpdateWindowTitle(void)
{
   if(Filename == NULL)
   {
      SDL_SetWindowTitle(Window, WINDOW_TITLE);
   }
   else
   {
      char * temp;
      const char * runner = Filename;
      const char * startOfFilename = Filename;
      while(*runner != '\0')
      {
         if(*runner == '/' || *runner == '\\')
         {
            startOfFilename = runner + 1;
         }
         runner ++;
      }
      temp = malloc(sizeof(WINDOW_TITLE) + strlen(startOfFilename) + 4);
      sprintf(temp, WINDOW_TITLE " (%s)", startOfFilename);
      SDL_SetWindowTitle(Window, temp);
      free(temp);
   }
}


inline static void updateDisplayTexture(SDL_Renderer * rend)
{
   if(imageTexture != NULL)
   {
      SDL_DestroyTexture(imageTexture);
   }
   imageTexture = SDL_CreateTextureFromSurface(rend, image);
}


static void SetCursorPosition(int x, int y)
{
   if(CursorWrap)
   {
      if(x >= 0)
      {
         CursorX = x % imageWidth;
      }
      else
      {
         CursorX = imageWidth - ((-x) % imageWidth);
      }
      
      if(y >= 0)
      {
         CursorY = y % imageHeight;
      }
      else
      {
         CursorY = imageHeight - ((-y) % imageHeight);
      }
   }
   else
   {
      if(x < 0)
      {
         CursorX = 0;
      }
      else if(x >= imageWidth)
      {
         CursorX = imageWidth - 1;
      }
      else
      {
         CursorX = x;
      }

      if(y < 0)
      {
         CursorY = 0;
      }
      else if(y >= imageHeight)
      {
         CursorY = imageHeight - 1;
      }
      else
      {
         CursorY = y;
      }
   }
   TextTexture_Write(&TTCursorPosition, "%4d %-4d", CursorX, CursorY);
}

static void MoveCursorPosition(int dx, int dy)
{
   SetCursorPosition(CursorX + dx, CursorY + dy);
}


void draw_pixel(SDL_Renderer * rend, int x, int y)
{
   struct colorRGB * c = PalletGetSelectedColor();
   SDLSurfaceDraw_SetRGB(image, x, y, c);
   updateDisplayTexture(rend);
}


static void previewSetScale(int newScale)
{
   if(newScale < 1) newScale = 1;
   
   if(PreviewScale != newScale)
   {
      PreviewScale = newScale;
      UIUpdateLayout();
   }
}

static void previewSetTile(int newTileX, int newTileY)
{
   int changeFlag = 0;
   
   if(newTileX >= 1 && newTileX != PreviewTileX)
   {
      PreviewTileX = newTileX;
      changeFlag = 1;
   }
   if(newTileY >= 1 && newTileY != PreviewTileY)
   {
      PreviewTileY = newTileY;
      changeFlag = 1;
   }
   if(changeFlag)
   {
      UIUpdateLayout();
   }
}

static void editorScaleSet(int newScale)
{
   if(newScale < 1) newScale = 1;
   
   if(scale != newScale)
   {
      scale = newScale;
      UIUpdateLayout();
   }
}

static void editorScaleUp(void)
{
   editorScaleSet(scale * 2);
}

static void editorScaleDown(void)
{
   if(scale > 1)
   {
      editorScaleSet(scale / 2);
   }
}

static void dumpParamList(const struct commandParserParamList * list)
{
   int i;
   const char * forceText;
   if(list->forceFlag)
   {
      forceText = "Forced (!)";
   }
   else
   {
      forceText = "Not Forced";
   }
   printf("Command %s (%d) called with %d arguments and is %s\n", list->command->cmd, 
                                                                  list->command->key, 
                                                                  list->count,
                                                                  forceText);
   
   for(i = 0; i < list->count; i++)
   {
      printf("Arg %d is: \"%.*s\"\n", i, (int)list->length[i], list->param[i]);
   }

}

static const char * trim(const char * str, size_t * length)
{
   const char * end = str + *length - 1;
   

   while((*end == ' ' || *end == '\t') && end >= str)
   {
      end --;
   }

   while((*str == ' ' || *str == '\t') && str <= end)
   {
      str ++;
   }

   *length = end - str + 1;
   return str;
}

static const char * getFileExtention(const char * str, size_t * length)
{
   const char * start = str + *length - 1;
   if(*start == '.')
   {
      *length = 0;
      return start;
   }
   start --;
   while(*start != '.' && start > str)
   {
      start --;
   }
   if(*start != '.')
   {
      *length = 0;
      return str;
   }
   start ++;
   *length = *length - (start - str);
   return start;
}

static char * stringClone(const char * str, size_t length)
{
   char * out;
   out = malloc(length + 1);
   memcpy(out, str, length);
   out[length] = '\0';
   return out;
}

static bool readDouble(const char * str, size_t length, double * value)
{
   char temp[128];
   if(value == NULL) return true;

   memcpy(temp, str, length);
   temp[length] = '\0';
   (*value) = strtod(temp, NULL);
   return true;
}

static bool readLongLong(const char * str, size_t length, long long * value)
{
   char temp[128];
   if(value == NULL) return true;
   memcpy(temp, str, length);
   temp[length] = '\0';
   (*value) = strtoll(temp, NULL, 0);
   return true;
}

#define COMMAND_TEST              1
#define COMMAND_WRITE_TO_FILE     2
#define COMMAND_EDIT_FILE         3
#define COMMAND_NEW_IMAGE         4
#define COMMNAD_QUIT              5
#define COMMAND_PREVIEW           6
#define COMMAND_PREVIEW_TILE      7
#define COMMAND_MOVE_ABS          8
#define COMMAND_MOVE_REL          9
#define COMMAND_PALLET_COLOR      10
#define COMMAND_PALLET_COLOR_HSV  11


const struct commandParserCommand CommandList[] = {
   { NULL,  "test",             NULL,   COMMAND_TEST              },
   { "w",   "write",            "...",  COMMAND_WRITE_TO_FILE     },
   { "e",   "edit",             "...",  COMMAND_EDIT_FILE         },
   { "n",   "new",              NULL,   COMMAND_NEW_IMAGE         },
   { "q",   "quit",             NULL,   COMMNAD_QUIT              },
   { NULL,  "preview",          NULL,   COMMAND_PREVIEW           },
   { NULL,  "preview-tile",     NULL,   COMMAND_PREVIEW_TILE      },
   { "M",   "move-abs",         NULL,   COMMAND_MOVE_ABS          },
   { "m",   "move-rel",         NULL,   COMMAND_MOVE_REL          },
   { "pc",  "pallet-color",     NULL,   COMMAND_PALLET_COLOR      },
   { "hsv", "pallet-color-hsv", NULL,   COMMAND_PALLET_COLOR_HSV  },
   { NULL,  NULL,               NULL,   0                         }
};

static bool ProccessCommand(const char * text, SDL_Renderer * rend)
{
   struct commandParserParamList list;
   enum commandParseResult result;
   long long tempLongLong;
   
   printf("ProcessCommand: \"%s\"\n", text);
   
   result = CommandParser_Parse(text, &list);
   
   printf("Result: %d\n", result);
   if(result == eCPR_CommandFound)
   {
      dumpParamList(&list);
      switch(list.command->key)
      {
      case COMMAND_TEST:
         StatusShow("Test \"%s\"", text);
         break;
      case COMMAND_WRITE_TO_FILE:
         if(list.count == 1)
         {
            const char * ext;
            size_t ext_length;
            list.param[0] = trim(list.param[0], &list.length[0]);
            ext_length = list.length[0];
            ext = getFileExtention(list.param[0], &ext_length);
            if(ext_length != 3 || memcmp("png", ext, ext_length) != 0)
            {
               StatusShow("Extention %.*s is not supported. Try png", (int)ext_length, ext);
               break;
            }

            if(Filename != NULL)
            {
               free(Filename);
            }
            Filename = stringClone(list.param[0], list.length[0]);
            UpdateWindowTitle();
         }
         if(Filename != NULL)
         {
            if(IMG_SavePNG(image, Filename))
            {
               StatusShow("Error: Failed to write image: %s", Filename);
            }
            else
            {
               UnsavedChanges = false;
               StatusShow("Image written to: %s", Filename);
            }
         }
         else
         {
            StatusShow("Failed to save, no filename specified");
         }
         break;
      case COMMAND_EDIT_FILE:
         if(list.count != 1)
         {
            StatusShow("Failed to load, no filename specified");
         }
         else if(UnsavedChanges && !list.forceFlag)
         {
            StatusShow("No, there are unsaved changes. Try e!");
         }
         else
         {
            SDL_Surface * newFile;
            char * filename;
            list.param[0] = trim(list.param[0], &list.length[0]);
            filename = stringClone(list.param[0], list.length[0]);
            newFile = IMG_Load(filename);
            
            if(newFile == NULL)
            {
               StatusShow("Failed to load file: %s", filename);
               free(filename);
            }
            else
            {
               SDL_FreeSurface(image);
               image = newFile;
               updateDisplayTexture(rend);
               if(Filename != NULL)
               {
                  free(Filename);
               }
               Filename = filename;
               UpdateWindowTitle();
               UnsavedChanges = false;
               imageWidth = image->w;
               imageHeight = image->h;
               SetCursorPosition(CursorX, CursorY);
               StatusShow("Loaded Image File: %s (%d, %d)", Filename, imageWidth, imageHeight);
            }
            
         }
         break;
      case COMMAND_NEW_IMAGE:
         if(UnsavedChanges && !list.forceFlag)
         {
            StatusShow("No, there are unsaved changes. Try n!");
         }
         else
         {
            int newWidth;
            int newHeight;
            SDL_Surface * newSurf;
            bool badParam = false;
            if(list.count >= 1)
            {
               readLongLong(list.param[0], list.length[0], &tempLongLong);
               if(tempLongLong > 0)
               {
                  newWidth = (int)tempLongLong;
               }
               else
               {
                  newWidth = 1;
               }
            }
            else
            {
               newWidth = 32;
            }
            
            if(list.count >= 2)
            {
               readLongLong(list.param[1], list.length[1], &tempLongLong);
               if(tempLongLong > 0)
               {
                  newHeight = (int)tempLongLong;
               }
               else
               {
                  newHeight = 1;
               }
            }
            else
            {
               newHeight = 32;
            }
            
            
            
            newSurf = SDL_CreateRGBSurface(0, newWidth, newHeight, 32, 0, 0, 0, 0);
            if(newSurf != NULL)
            {
               SDL_FreeSurface(image);
               image = newSurf;
               UnsavedChanges = false;
               if(Filename != NULL)
               {
                  free(Filename);
                  Filename = NULL;
               }
               imageWidth = newWidth;
               imageHeight = newHeight;
               updateDisplayTexture(rend);
               SetCursorPosition(CursorX, CursorY);
               UpdateWindowTitle();
               StatusShow("New Image (%d, %d) created", imageWidth, imageHeight);
            }
            else
            {
               StatusShow("Error: Failed to create new texture");
            }
         }
         break;
      case COMMNAD_QUIT:
         if(UnsavedChanges && !list.forceFlag)
         {
            StatusShow("No, there are unsaved changes. Try q!");
         }
         else
         {
            Running = false;
         }
         break;
      case COMMAND_PREVIEW:
         if(list.count >= 1)
         {
            readLongLong(list.param[0], list.length[0], &tempLongLong);
            if(tempLongLong >= 1)
            {
               previewSetScale((int)tempLongLong);
            }
            else
            {
               previewSetScale(1);
            }
         }
         else
         {
            previewSetScale(2);
         }
         if(list.count >= 2)
         {
            readLongLong(list.param[1], list.length[1], &tempLongLong);
            if(tempLongLong >= 1)
            {
               previewSetTile(tempLongLong, -1);
            }
            else
            {
               previewSetTile(1, -1);
            }
         }

         if(list.count >= 3)
         {
            readLongLong(list.param[2], list.length[2], &tempLongLong);
            if(tempLongLong >= 1)
            {
               previewSetTile(-1, tempLongLong);
            }
            else
            {
               previewSetTile(-1, 1);
            }
         }
         break;
      case COMMAND_PREVIEW_TILE:

         if(list.count >= 1)
         {
            readLongLong(list.param[0], list.length[0], &tempLongLong);
            if(tempLongLong >= 1)
            {
               previewSetTile(tempLongLong, -1);
            }
            else
            {
               previewSetTile(1, -1);
            }
         }
         else
         {
            previewSetTile(1, 1);
         }

         if(list.count >= 2)
         {
            readLongLong(list.param[1], list.length[1], &tempLongLong);
            if(tempLongLong >= 1)
            {
               previewSetTile(-1, tempLongLong);
            }
            else
            {
               previewSetTile(-1, 1);
            }
         }
         break;
      case COMMAND_MOVE_ABS:
         if(list.count >= 2)
         {
            int newX, newY;
            readLongLong(list.param[0], list.length[0], &tempLongLong);
            newX = (int)tempLongLong;
            
            readLongLong(list.param[1], list.length[1], &tempLongLong);
            newY = (int)tempLongLong;

            SetCursorPosition(newX, newY);
         }
         if(list.count >= 3)
         {
            readLongLong(list.param[2], list.length[2], &tempLongLong);
            PalletSetColumn(tempLongLong);
         }
         break;
      case COMMAND_MOVE_REL:
         {
            int dx, dy, dp, newPalletIndex;
            if(list.count >= 1)
            {
               int newPos;
               readLongLong(list.param[0], list.length[0], &tempLongLong);
               dx = (int)tempLongLong;
            }
            else
            {
               dx = 0;
            }
            if(list.count >= 2)
            {
               int newPos;
               readLongLong(list.param[1], list.length[1], &tempLongLong);
               dy = (int)tempLongLong;
            }
            else
            {
               dy = 0;
            }
            
            MoveCursorPosition(dx, dy);
            
            if(list.count >= 3)
            {
               readLongLong(list.param[2], list.length[2], &tempLongLong);
               dp = (int)tempLongLong;
            }
            else
            {
               dp = 0;
            }
            
            newPalletIndex = dp + PalletX;
            
            if(newPalletIndex >= 0)
            {
               PalletSetColumn(newPalletIndex % PALLET_COLUMNS_SIZE);
            }
            else
            {
               PalletSetColumn(PALLET_COLUMNS_SIZE - ((-newPalletIndex) % PALLET_COLUMNS_SIZE));
            }
         }
         break;
      case COMMAND_PALLET_COLOR:
         if(list.count == 0)
         {
            struct colorRGB * c = PalletGetSelectedColor();
            int colorCompressed = (c->r << 24) | 
                                  (c->g << 16) | 
                                  (c->b << 8)  | 
                                  c->a;
         
            StatusShow("Current Color %d %d %d %d (0x%08X)\n", c->r, 
                                                               c->g, 
                                                               c->b, 
                                                               c->a, 
                                                               colorCompressed);
         }
         else if(list.count == 1)
         {
            struct colorRGB * c = PalletGetSelectedColor();
            readLongLong(list.param[0], list.length[0], &tempLongLong);
            c->r = (tempLongLong >> 24) & 0xFF;
            c->g = (tempLongLong >> 16) & 0xFF;
            c->b = (tempLongLong >> 8) & 0xFF;
            c->a = tempLongLong & 0xFF;
         }
         else
         {
            struct colorRGB * c = PalletGetSelectedColor();
            readLongLong(list.param[0], list.length[0], &tempLongLong);
            c->r = (Uint8)tempLongLong;
            if(list.count >= 2)
            {
               readLongLong(list.param[1], list.length[1], &tempLongLong);
               c->g = (Uint8)tempLongLong;
            }
            else
            {
               c->g = 0;
            }
            if(list.count >= 3)
            {
               readLongLong(list.param[2], list.length[2], &tempLongLong);
               c->b = (Uint8)tempLongLong;
            }
            else
            {
               c->b = 0;
            }
            if(list.count >= 4)
            {
               readLongLong(list.param[3], list.length[3], &tempLongLong);
               c->a = (Uint8)tempLongLong;
            }
            else
            {
               c->a = SDL_ALPHA_OPAQUE;
            }
         }
         break;
      case COMMAND_PALLET_COLOR_HSV:
         if(UIPickerHSVEnabled)
         {
            StateSetEdit();
         }
         else
         {
            StateSetHSVPicker();
         }
         UIUpdateLayout();
         break;
      default:
         printf("Error: Unexpected Command %s (%d)\n", list.command->cmd, list.command->key);
         StatusShow("Error: Unhandled Command %s (%d)", list.command->cmd, list.command->key);
         break;
      }
   }
   else if(result == eCPR_NotACommand && strlen(text) > 0)
   {
      StatusShow("Invalid Command Form: \"%s\"", text);
   }
   else if(result == eCPR_CommandNotFound)
   {
      StatusShow("Invalid Command: \"%.*s\"", (int)list.cmdTextLength, list.cmdTextStart);
   }
   return result == eCPR_CommandFound;
}

// Externing Font Data
extern unsigned char Inconsolata_Regular_ttf[];
extern unsigned int Inconsolata_Regular_ttf_len;


static void render(SDL_Renderer * rend);

static void handleSDLEvent(SDL_Event * event, SDL_Renderer * rend);

static void update(double seconds);

int main(int argc, char* argv[])
{   
   SDL_Renderer * rend;
   SDL_Event event;
   Uint32 prevTicks;

   TTF_Font * font;
   
   SDL_Init(SDL_INIT_VIDEO);              // Initialize SDL2
   TTF_Init();
   IMG_Init(IMG_INIT_PNG | IMG_INIT_JPG | IMG_INIT_TIF);

   // Create an application window with the following settings:
   Window = SDL_CreateWindow(
      WINDOW_TITLE,                      // window title
      SDL_WINDOWPOS_UNDEFINED,           // initial x position
      SDL_WINDOWPOS_UNDEFINED,           // initial y position
      ScreenWidth,                       // width, in pixels
      ScreenHeight,                      // height, in pixels
      SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);
   
   // Check that the window was successfully created
   if (Window == NULL) {
      // In the case that the window could not be made...
      printf("Could not create window: %s\n", SDL_GetError());
      return 1;
   }
   
   rend = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
   
   image = SDL_CreateRGBSurface(0, imageWidth, imageHeight, 32, 0, 0, 0, 0);
   updateDisplayTexture(rend);
   

   font = TTF_OpenFontRW(SDL_RWFromConstMem(Inconsolata_Regular_ttf, 
                                            Inconsolata_Regular_ttf_len), 1,
                         20);
   CommandBuffer_Init(font);
   CommandParser_Init(CommandList);
   TextTexture_Init(&TTCursorPosition, font);
   TextTexture_Init(&TTStatus, font);
   SetCursorPosition(0, 0);

   PalletInit(PalletColumnsSize);
   set_pallet_to_defaults();
   UIPickerHSV_Init(rend);

   UIUpdateLayout();
   
   SDL_StartTextInput();
   
   StateSetEdit();

   Running = true;
   prevTicks = SDL_GetTicks();
   while(Running)
   {
      Uint32 ticks;
      while(SDL_PollEvent(&event))
      {
         handleSDLEvent(&event, rend);
      }

      ticks = SDL_GetTicks();
      update((double)(ticks - prevTicks) / 1000.0);
      prevTicks = ticks;

      render(rend);
      
      // Show to user
      SDL_RenderPresent(rend);
      
   }
   PalletCleanup();

   SDL_StopTextInput();
   
   UIPickerHSV_Destroy();
   SDL_DestroyTexture(imageTexture);
   SDL_FreeSurface(image);
   
   TextTexture_Destroy(&TTStatus);
   TextTexture_Destroy(&TTCursorPosition);   
   CommandParser_Destroy();
   CommandBuffer_Destroy();  
   TTF_CloseFont(font);
   
   // Close and destroy the window
   SDL_DestroyWindow(Window);
   
   // Clean up
   IMG_Quit();
   TTF_Quit();
   SDL_Quit();
   return 0;
}

static double slew(double value, double target, double amount)
{
   double result;
   if(value > target)
   {
      if((value - amount) < target)
      {
         result = target;
      }
      else
      {
         result = value - amount;
      }
   }
   else if(value < target)
   {
      if((value + amount) > target)
      {
         result = target;
      }
      else
      {
         result = value + amount;
      }
   }
   else
   {
      result = value;
   }
   return result;
}

static void update(double seconds)
{
   int i;
   const double percentSpeed = 8.0;
   for(i = 0; i < PALLET_COLUMNS_SIZE; i++)
   {
      PalletColumns[i].offset = slew(PalletColumns[i].offset, 0, 
                                     percentSpeed * seconds);
   }
}

static void handleKIEEvent(struct kieInputEvent * event, SDL_Renderer * rend);

static void handleSDLEvent(SDL_Event * event, SDL_Renderer * rend)
{
   const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);
   static Uint32 lastLeftShiftKeyUpTimestamp = 0;
   struct kieInputEvent kieEvent;

   kieEvent.type = eKIET_None;
   kieEvent.fast = false;
   kieEvent.paintIsPressed = keyboardState[SDL_SCANCODE_SPACE];

   CommandBuffer_HandelSDLEvent(event);
   if(event->type == SDL_QUIT)
   {
      Running = false;
   }
   else if(event->type == SDL_WINDOWEVENT)
   {
      if(event->window.event == SDL_WINDOWEVENT_RESIZED)
      {
         ScreenWidth  = event->window.data1;
         ScreenHeight = event->window.data2;
         UIUpdateLayout();
      }
   }
   else if(event->type == SDL_KEYDOWN)
   {
      
      if(CommandBuffer_IsTextInputActive())
      {
         switch(event->key.keysym.sym)
         {
         case SDLK_ESCAPE:
            CommandBuffer_StopTextInput();
            break;
         case SDLK_RETURN:
            CommandBuffer_StopTextInput();
            if(ProccessCommand(CommandBuffer_GetText(), rend))
            {
               CommandBuffer_AddToHistroy();
            }
            break;               
         }
      }
      else
      {
         if(keyboardState[SDL_SCANCODE_LSHIFT] || 
            lastLeftShiftKeyUpTimestamp == event->key.timestamp) // Sneaky hack for windows keypad nonsens
         {
            kieEvent.fast = true;
            switch(event->key.keysym.scancode)
            {
            case SDL_SCANCODE_KP_7:
               kieEvent.type = eKIET_CursorUpLeft;
               break;
            case SDL_SCANCODE_KP_8:
            case SDL_SCANCODE_UP:
               kieEvent.type = eKIET_CursorUp;
               break;
            case SDL_SCANCODE_KP_9:
               kieEvent.type = eKIET_CursorUpRight;
               break;
            case SDL_SCANCODE_KP_6:
            case SDL_SCANCODE_RIGHT:
               kieEvent.type = eKIET_CursorRight;
               break;
            case SDL_SCANCODE_KP_3:
               kieEvent.type = eKIET_CursorDownRight;
               break;
            case SDL_SCANCODE_KP_2:
            case SDL_SCANCODE_DOWN:
               kieEvent.type = eKIET_CursorDown;
               break;
            case SDL_SCANCODE_KP_1:
               kieEvent.type = eKIET_CursorDownLeft;
               break;
            case SDL_SCANCODE_KP_4:
            case SDL_SCANCODE_LEFT:
               kieEvent.type = eKIET_CursorLeft;
               break;

            default:
               //printf("Unknown Keycode Shift: %d, Mod: %d\n", event.key.keysym.scancode, event.key.keysym.mod);
               break;
            }
         }
         else
         {
            kieEvent.fast = false;
            switch(event->key.keysym.scancode)
            {
            case SDL_SCANCODE_KP_7:
               kieEvent.type = eKIET_CursorUpLeft;
               break;
            case SDL_SCANCODE_KP_8:
               kieEvent.type = eKIET_CursorUp;
               break;
            case SDL_SCANCODE_KP_9:
               kieEvent.type = eKIET_CursorUpRight;
               break;
            case SDL_SCANCODE_KP_6:
               kieEvent.type = eKIET_CursorRight;
               break;
            case SDL_SCANCODE_KP_3:
               kieEvent.type = eKIET_CursorDownRight;
               break;
            case SDL_SCANCODE_KP_2:
               kieEvent.type = eKIET_CursorDown;
               break;
            case SDL_SCANCODE_KP_1:
               kieEvent.type = eKIET_CursorDownLeft;
               break;
            case SDL_SCANCODE_KP_4:
               kieEvent.type = eKIET_CursorLeft;
               break;

            default:
               //printf("Unknown Keycode: %d, Mod: %d\n", event.key.keysym.scancode, event.key.keysym.mod);
               break;
            }
         }
         

         switch(event->key.keysym.sym)
         {
         case SDLK_SPACE:
            kieEvent.type = eKIET_Paint;
            break;
         case SDLK_1:
            kieEvent.type = eKIET_PalletIndex0;
            break;
         case SDLK_2:
            kieEvent.type = eKIET_PalletIndex1;
            break;
         case SDLK_3:
            kieEvent.type = eKIET_PalletIndex2;
            break;
         case SDLK_4:
            kieEvent.type = eKIET_PalletIndex3;
            break;
         case SDLK_5:
            kieEvent.type = eKIET_PalletIndex4;
            break;
         case SDLK_6:
            kieEvent.type = eKIET_PalletIndex5;
            break;
         case SDLK_7:
            kieEvent.type = eKIET_PalletIndex6;
            break;
         case SDLK_8:
            kieEvent.type = eKIET_PalletIndex7;
            break;
         case SDLK_9:
            kieEvent.type = eKIET_PalletIndex8;
            break;
         case SDLK_0:
            kieEvent.type = eKIET_PalletIndex9;
            break;
         case SDLK_a:
            kieEvent.type = eKIET_PalletLeft;
            break;
         case SDLK_d:
            kieEvent.type = eKIET_PalletRight;
            break;
         case SDLK_w:
            kieEvent.type = eKIET_PalletUp;
            break;
         case SDLK_s:
            kieEvent.type = eKIET_PalletDown;
            break;
         case SDLK_ESCAPE:
            if(UIPickerHSVEnabled)
            {
               StateSetEdit();
            }
            break;

         default:
            //printf("Unknown Key: %d\n", event.key.keysym.sym);
            break;
         }
      }
      
      
   }
   else if(event->type == SDL_KEYUP)
   {
      switch(event->key.keysym.sym)
      {
      case SDLK_LSHIFT:
         lastLeftShiftKeyUpTimestamp = event->key.timestamp;
         break;
      }
   }
   else if(event->type == SDL_TEXTINPUT)
   {
      if(!CommandBuffer_IsTextInputActive())
      {
         switch(event->text.text[0])
         {
         case ':':
            CommandBuffer_StartTextInput();
            CommandBuffer_SetText(":");
            StatusHide();
            break;
         case '+':
            kieEvent.type = eKIET_EditorZoomIn;
            break;
         case '-':
            kieEvent.type = eKIET_EditorZoomOut;
            break;
         }
      }
   }

   if(kieEvent.type != eKIET_None)
   {
      handleKIEEvent(&kieEvent, rend);
   }
}

static inline int computeFastMoveDistance()
{
   int smallestImageDimension;
   if(imageWidth > imageHeight)
   {
      smallestImageDimension = imageHeight;
   }
   else
   {
      smallestImageDimension = imageWidth;
   }
   return smallestImageDimension / 4;
}

static void handleKIEEvent(struct kieInputEvent * event, SDL_Renderer * rend)
{

   int offsetX, offsetY;
   struct paletColor * pc;

   int moveDistance;

   if(UIPickerHSVEnabled)
   {
      UIPickerHSV_InputEvent(event);
      if(UIPickerHSV_HasColorChanged())
      {
         struct colorRGB * c = PalletGetSelectedColor();
         const struct colorHSV * pickerColor = UIPickerHSV_GetHSV();
         Color_HSVToRGB(c, pickerColor);
      }
   }

   if(EditorInputEnabled)
   {
      Input_GetCursorVector(event->type, &offsetX, &offsetY);
      if(event->fast)
      {
         moveDistance = computeFastMoveDistance();
         offsetX *= moveDistance;
         offsetY *= moveDistance;
      }

      switch(event->type)
      {
      case eKIET_Paint:
         draw_pixel(rend, CursorX, CursorY);
         break;
      case eKIET_EditorZoomIn:
         editorScaleUp();
         break;
      case eKIET_EditorZoomOut:
         editorScaleDown();
         break;
      default:
         // Empty on purpose
         break;
      }

      if(offsetX != 0 || offsetY != 0)
      {
         MoveCursorPosition(offsetX, offsetY);
         if(event->paintIsPressed)
         {
            draw_pixel(rend, CursorX, CursorY);
         }
      }
   }

   if(PalletInputEnabled)
   {
      switch(event->type)
      {
      case eKIET_PalletUp:
         PalletMoveUp();
         break;
      case eKIET_PalletDown:
         PalletMoveDown();
         break;
      case eKIET_PalletLeft:
         PalletMoveLeft();
         break;
      case eKIET_PalletRight:
         PalletMoveRight();
         break;
      case eKIET_PalletIndex0:
         PalletSetColumn(0);
         break;
      case eKIET_PalletIndex1:
         PalletSetColumn(1);
         break;
      case eKIET_PalletIndex2:
         PalletSetColumn(2);
         break;
      case eKIET_PalletIndex3:
         PalletSetColumn(3);
         break;
      case eKIET_PalletIndex4:
         PalletSetColumn(4);
         break;
      case eKIET_PalletIndex5:
         PalletSetColumn(5);
         break;
      case eKIET_PalletIndex6:
         PalletSetColumn(6);
         break;
      case eKIET_PalletIndex7:
         PalletSetColumn(7);
         break;
      case eKIET_PalletIndex8:
         PalletSetColumn(8);
         break;
      case eKIET_PalletIndex9:
         PalletSetColumn(9);
         break;
      default:
         // Empty on purpose
         break;
      }
   }
}

static void drawUIFrame(SDL_Renderer * rend, const struct uiLayoutBox * comp)
{
   SDL_Rect rect;
   
   // Clear the clipping 
   SDL_RenderSetClipRect(rend, NULL);
   
   // draw background
   SDL_SetRenderDrawColor(rend, UI_COLOR_BACKGROUND);
   rect.x = comp->computedOffset.x - 3;
   rect.y = comp->computedOffset.y - 3;
   rect.w = comp->computedDim.width + 6;
   rect.h = comp->computedDim.height + 6;
   SDL_RenderFillRect(rend, &rect);
   
   // Draw Border
   SDL_SetRenderDrawColor(rend, UI_COLOR_FRAME);
   rect.x = comp->computedOffset.x - 2;
   rect.y = comp->computedOffset.y - 2;
   rect.w = comp->computedDim.width + 4;
   rect.h = comp->computedDim.height + 4;
   SDL_RenderDrawRect(rend, &rect);
   
   // Set Clipping for drawing the component
   rect.x = comp->computedOffset.x;
   rect.y = comp->computedOffset.y;
   rect.w = comp->computedDim.width;
   rect.h = comp->computedDim.height;
   SDL_RenderSetClipRect(rend, &rect);   
}

static void renderEditorArea(SDL_Renderer * rend)
{
   SDL_Rect rect;
   SDL_Rect imgRect;
   struct colorRGB c;
   
   const int halfScale = scale / 2;
   const int editableImageWidth  = imageWidth  * scale;
   const int editableImageHeight = imageHeight * scale;
   
   const int cursorOnImageX = CursorX * scale;
   const int cursorOnImageY = CursorY * scale;
   
   const int cursorCenterOnImageX = (cursorOnImageX) + halfScale;
   const int cursorCenterOnImageY = (cursorOnImageY) + halfScale;
   
   const struct uiLayoutBox * comp = &UILayoutEditor.component;
   
   const int frameCenterX = comp->computedOffset.x + (comp->computedDim.width  / 2);
   const int frameCenterY = comp->computedOffset.y + (comp->computedDim.height / 2);
   
   const int componentRight  = comp->computedOffset.x + comp->computedDim.width;
   const int componentBottom = comp->computedOffset.y + comp->computedDim.height;
   
   drawUIFrame(rend, comp);
   
   // Draw Image
   imgRect.x = frameCenterX - cursorCenterOnImageX;
   
   if(imgRect.x + editableImageWidth < componentRight)
   {
      imgRect.x =  componentRight - editableImageWidth;
   }
   if(imgRect.x > comp->computedOffset.x)
   {
      imgRect.x = comp->computedOffset.x;
   }
   imgRect.y = frameCenterY - cursorCenterOnImageY;
   
   if(imgRect.y + editableImageHeight < componentBottom)
   {
      imgRect.y =  componentBottom - editableImageHeight;
   }
   if(imgRect.y > comp->computedOffset.y)
   {
      imgRect.y = comp->computedOffset.y;
   }

   imgRect.w = editableImageWidth;
   imgRect.h = editableImageHeight;
   
   SDL_RenderCopy(rend, imageTexture, NULL, &imgRect);

   // Draw Cursor
   SDLSurfaceDraw_GetRGB(image, CursorX, CursorY, &c);
   c.r = (c.r + 128) % 256;
   c.b = (c.b + 128) % 256;
   c.g = (c.g + 128) % 256;
   SDL_SetRenderDrawColor(rend, c.r, c.g, c.b, SDL_ALPHA_OPAQUE);
   rect.x = imgRect.x + cursorOnImageX;
   rect.y = imgRect.y + cursorOnImageY;
   rect.w = scale;
   rect.h = scale;
   SDL_RenderDrawRect(rend, &rect);   
}

static void renderPreview(SDL_Renderer * rend)
{
   int tx, ty;
   SDL_Rect rect;
   int previewStartX, previewStartY;
   const int previewWidth  = imageWidth  * PreviewScale;
   const int previewHeight = imageHeight * PreviewScale;
   
   const int previewTiledWidth  = previewWidth  * PreviewTileX;
   const int previewTiledHeight = previewHeight * PreviewTileY;
   
   const struct uiLayoutBox * comp = &UILayoutPreview.component;
   
   const int halfScale = PreviewScale / 2;
   
   const int cursorOnImageX = CursorX * PreviewScale;
   const int cursorOnImageY = CursorY * PreviewScale;
   
   const int cursorCenterOnImageX = (cursorOnImageX) + halfScale;
   const int cursorCenterOnImageY = (cursorOnImageY) + halfScale;
   
   const int frameCenterX = comp->computedOffset.x + (comp->computedDim.width  / 2);
   const int frameCenterY = comp->computedOffset.y + (comp->computedDim.height / 2);
   
   const int componentRight  = comp->computedOffset.x + comp->computedDim.width;
   const int componentBottom = comp->computedOffset.y + comp->computedDim.height;   
   
   drawUIFrame(rend, comp);
   
   previewStartX = frameCenterX - cursorCenterOnImageX;
   
   if(previewStartX + previewTiledWidth < componentRight)
   {
      previewStartX =  componentRight - previewTiledWidth;
   }
   if(previewStartX > comp->computedOffset.x)
   {
      previewStartX = comp->computedOffset.x;
   }
   previewStartY = frameCenterY - cursorCenterOnImageY;
   
   if(previewStartY + previewTiledHeight < componentBottom)
   {
      previewStartY =  componentBottom - previewTiledHeight;
   }
   if(previewStartY > comp->computedOffset.y)
   {
      previewStartY = comp->computedOffset.y;
   }
   
   for(ty = 0; ty < PreviewTileY; ty ++)
   {
      for(tx = 0; tx < PreviewTileX; tx ++)
      {
         rect.x = previewStartX + (tx * previewWidth);
         rect.y = previewStartY + (ty * previewHeight);
         rect.w = previewWidth;
         rect.h = previewHeight;
         
         SDL_RenderCopy(rend, imageTexture, NULL, &rect);
      }
   }

} 

static struct colorRGB * getRotatedPalletColor(int x, int y, 
                                               int index, int putAtRow)
{
   int shift = putAtRow - index;
   int palletY = y - shift;
   
   if(palletY < 0)
   {
      palletY += PalletColumnsSize;
   }
   else if(palletY >= PalletColumnsSize)
   {
      palletY -= PalletColumnsSize;
   }
   return PalletGetColorAt(x, palletY);
}

static void renderPallet(SDL_Renderer * rend)
{
   SDL_Rect rect;
   int pcx, pcy;
   const struct uiLayoutBox * comp = &UILayoutPallet.component;

   const int palletWidthInColors  = PALLET_COLUMNS_SIZE;
   const int palletHeightInColors = PalletColumnsSize;
   
   const int palletOptionWidth  = comp->computedDim.width  / palletWidthInColors;
   const int palletOptionHeight = comp->computedDim.height / palletHeightInColors;

   const int putAtRow = PalletColumnsSize / 2;

   drawUIFrame(rend, comp);
   
   // Draw Colors
   for(pcx = 0; pcx < palletWidthInColors; pcx ++)
   {
      int shiftY;
      int index;
      struct colorRGB * c;
      if(pcx == PalletX)
      {
         index = PalletY;
      }
      else
      {
         index = PalletColumns[pcx].index;
      }
      if(PalletLoadPreviousRow)
      {
         shiftY = PalletColumns[pcx].offset * palletOptionHeight;
         if(shiftY != 0)
         {
            if(shiftY > 0)
            {
               c = getRotatedPalletColor(pcx, PalletColumnsSize - 1, index, putAtRow);
               SDL_SetRenderDrawColor(rend, c->r, c->g, c->b, c->a);
               rect.x = comp->computedOffset.x + (pcx * palletOptionWidth)  + 1;
               rect.y = comp->computedOffset.y + (-1  * palletOptionHeight) + 1 + shiftY;
               rect.w = palletOptionWidth  - 2;
               rect.h = palletOptionHeight - 2;  
               SDL_RenderFillRect(rend, &rect);         
            }
            else
            {
               c = getRotatedPalletColor(pcx, 0, index, putAtRow);
               SDL_SetRenderDrawColor(rend, c->r, c->g, c->b, c->a);
               rect.x = comp->computedOffset.x + (pcx               * palletOptionWidth)  + 1;
               rect.y = comp->computedOffset.y + (PalletColumnsSize * palletOptionHeight) + 1 + shiftY;
               rect.w = palletOptionWidth  - 2;
               rect.h = palletOptionHeight - 2;  
               SDL_RenderFillRect(rend, &rect);         
            }
         }
      }
      else
      {
         shiftY = 0;
      }


      for(pcy = 0; pcy < palletHeightInColors; pcy ++)
      {
         // Draw Color
         if(PalletLoadPreviousRow)
         {
            c = getRotatedPalletColor(pcx, pcy, index, putAtRow);
         }
         else
         {
            c = PalletGetColorAt(pcx, pcy);
         }
         SDL_SetRenderDrawColor(rend, c->r, c->g, c->b, c->a);
         rect.x = comp->computedOffset.x + (pcx * palletOptionWidth)  + 1;
         rect.y = comp->computedOffset.y + (pcy * palletOptionHeight) + 1 + shiftY;
         rect.w = palletOptionWidth  - 2;
         rect.h = palletOptionHeight - 2;  
         SDL_RenderFillRect(rend, &rect);         
         
         // Draw Outline
         SDL_SetRenderDrawColor(rend, 0, 0, 0, SDL_ALPHA_OPAQUE);
         SDL_RenderDrawRect(rend, &rect);
      }

   }
   
   // Draw Color Cursor
   
   rect.x = comp->computedOffset.x + (PalletX  * palletOptionWidth);
   if(PalletLoadPreviousRow)
   {
      rect.y = comp->computedOffset.y + (putAtRow * palletOptionHeight);
   }
   else
   {
      rect.y = comp->computedOffset.y + (PalletY * palletOptionHeight);
   }
   rect.w = palletOptionWidth;
   rect.h = palletOptionHeight;  
   SDL_SetRenderDrawColor(rend, UI_COLOR_FRAME);
   SDL_RenderDrawRect(rend, &rect);
}

static void renderSimpleTextFrame(SDL_Renderer * rend, 
                                  struct textTexture * text, 
                                  const struct uiLayoutBox * comp)
{
   SDL_Rect rect;
   drawUIFrame(rend, comp);
   
   // Draw Text Background
   rect.x = comp->computedOffset.x;
   rect.y = comp->computedOffset.y;
   rect.w = comp->computedDim.width;
   rect.h = comp->computedDim.height;
   SDL_SetRenderDrawColor(rend, UI_COLOR_TEXTBACKGROUND);
   SDL_RenderFillRect(rend, &rect);
   
   // Draw Text
   TextTexture_Render(text, rend, rect.x, rect.y);

}

static void renderCommandBuffer(SDL_Renderer * rend)
{
   SDL_Rect rect;
   const struct uiLayoutBox * comp = &UILayoutStatus.component;
   
   drawUIFrame(rend, comp);

   // Draw Text Background
   rect.x = comp->computedOffset.x;
   rect.y = comp->computedOffset.y;
   rect.w = comp->computedDim.width;
   rect.h = comp->computedDim.height;
   SDL_SetRenderDrawColor(rend, UI_COLOR_TEXTBACKGROUND);
   SDL_RenderFillRect(rend, &rect);
   
   CommandBuffer_RenderTextAtHeight(rend, comp->computedOffset.x, 
                                          comp->computedOffset.y, 
                                          comp->computedDim.height);

}

static void render(SDL_Renderer * rend)
{ 
   SDL_SetRenderDrawColor(rend, UI_COLOR_BACKGROUND);
   SDL_RenderClear(rend);

   renderEditorArea(rend);
   
   renderPreview(rend);

   renderPallet(rend);
   
   if(CommandBuffer_IsTextInputActive())
   {
      renderCommandBuffer(rend);
   }
   else if(StatusVisible)
   {
      // Render Status Text
      renderSimpleTextFrame(rend, &TTStatus, &UILayoutStatus.component);
   }
   
   // Cursor Position Text
   renderSimpleTextFrame(rend, &TTCursorPosition, &UILayoutCoord.component);
     
   
   if(UIPickerHSVEnabled)
   {
      drawUIFrame(rend, &UILayoutColorPicker.component);
      UIPickerHSV_Render(rend, &UILayoutColorPicker.component);
   }

   if(DebugRenderUIBoxes)
   {
      SDL_Rect rect;
      // Render UI Boxes
      SDL_RenderSetClipRect(rend, NULL);
      SDL_SetRenderDrawColor(rend, 0, 255, 0, 255);
      UILayout_CompToSDLRect(&rect, &UILayoutEditor);
      SDL_RenderDrawRect(rend, &rect);
      UILayout_CompToSDLRect(&rect, &UILayoutPreview);
      SDL_RenderDrawRect(rend, &rect);
      UILayout_CompToSDLRect(&rect, &UILayoutPallet);
      SDL_RenderDrawRect(rend, &rect);
      UILayout_CompToSDLRect(&rect, &UILayoutStatus);
      SDL_RenderDrawRect(rend, &rect);
      UILayout_CompToSDLRect(&rect, &UILayoutCoord);
      SDL_RenderDrawRect(rend, &rect);
   }
}

