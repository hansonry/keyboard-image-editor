#include <string.h>
#include "CommandParser.h"


static const struct commandParserCommand * Commands = NULL;

void CommandParser_Init(const struct commandParserCommand * commands)
{
   Commands = commands;
}

void CommandParser_Destroy(void)
{
   Commands = NULL;
}

static const char * findNextNonWhitespace(const char * start)
{
   while((*start == ' ' || *start == '\t') && *start != '\0')
   {
      start++;
   }
   return start;
}
static const char * findNextWhitespace(const char * start)
{
   while(*start != ' ' && *start != '\t' && *start != '\0')
   {
      start++;
   }
   return start;
}

static const char * findNextOccurrenceOf(const char * start, const char * token)
{
   const char * tokenSoFar;
   while(*start != '\0')
   {
      tokenSoFar = token;
      while(*start == *tokenSoFar && *start != '\0' && *tokenSoFar != '\0')
      {
         start++;
         tokenSoFar ++;
      }
      if(*start == '\0' || *tokenSoFar == '\0')
      {
         break;
      }
      start++;
   }
   
   if(*start == '\0' && *tokenSoFar != '\0')
   {
      return start;
   }
   return start - strlen(token);
}

static const struct commandParserCommand * searchForCommand(const char * str, size_t length, int * index)
{
   const struct commandParserCommand * cmd = Commands;
   const struct commandParserCommand * found = NULL;
   int i = 0;
   while(cmd->cmd != NULL || cmd->longCmd != NULL)
   {
      if((cmd->cmd != NULL &&
          strlen(cmd->cmd) == length &&
          memcmp(cmd->cmd, str, length) == 0) ||
         (cmd->longCmd != NULL &&
          strlen(cmd->longCmd) == length &&
          memcmp(cmd->longCmd, str, length) == 0))
      {
         found = cmd;
         break;
      }
      i++;
      cmd ++;
   }
   
   if(found == NULL)
   {
      i = -1;
   }
   if(index != NULL)
   {
      (*index) = i;
   }
   
   return found;
}

/*
   const struct commandParserCommand * command;
   int commandIndex;
   int count;
   const char * param[COMMANDPARSER_MAX_PARAMS];
   size_t length[COMMANDPARSER_MAX_PARAMS];
*/
enum commandParseResult CommandParser_Parse(const char * command, struct commandParserParamList * list)
{
   size_t len;
   const char * endOfCmd;
   const char * startOfParam;
   // Find first space for command bounds
   list->command = NULL;
   list->commandIndex = -1;
   list->count = 0;
   list->forceFlag = 0;
   list->cmdTextStart = command;
   list->cmdTextLength = 0;
   
   len = strlen(command);
   if(len == 0 || command[0] != ':')
   {
      return eCPR_NotACommand;
   }
 
   list->cmdTextStart = findNextNonWhitespace(&command[1]);
   if(*list->cmdTextStart == '\0')
   {
      return eCPR_EmptyCommand;
   }
   endOfCmd = findNextWhitespace(list->cmdTextStart);
   if(*(endOfCmd - 1) == '!')
   {
      list->forceFlag = 1;
   }
   else
   {
      list->forceFlag = 0;
   }
   list->cmdTextLength = endOfCmd - list->cmdTextStart - (list->forceFlag ? 1 : 0);
   list->command = searchForCommand(list->cmdTextStart, 
                                    list->cmdTextLength, 
                                    &list->commandIndex);
   
   if(list->command == NULL)
   {
      return eCPR_CommandNotFound;
   }
   
   startOfParam = findNextNonWhitespace(endOfCmd);

   while(*startOfParam != '\0')
   {
      const char * endOfParam;
      const char * startOfNextParam;
      if(list->command->paramSeperator == NULL)
      {
         endOfParam = findNextWhitespace(startOfParam);
         startOfNextParam = findNextNonWhitespace(endOfParam);
      }
      else
      {
         endOfParam = findNextOccurrenceOf(startOfParam, 
                                           list->command->paramSeperator);
         if(*endOfParam != '\0')
         {
            startOfNextParam = endOfParam + strlen(list->command->paramSeperator);
         }
         else
         {
            startOfNextParam = endOfParam;
         }
      }
      

      if(list->count < COMMANDPARSER_MAX_PARAMS)
      {
         list->param[list->count]  = startOfParam;
         list->length[list->count] = endOfParam - startOfParam;
         list->count ++;
      }
      
      startOfParam = startOfNextParam;
      
   }
   
   
   return eCPR_CommandFound;
}

