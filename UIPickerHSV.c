#include "UIPickerHSV.h"
#include "SDLSurfaceDraw.h"

static struct colorHSV Color = { 180, 0.5, 0.5, 1 };
static bool HueDirty = true;
static bool ColorChangedFlag = true;

static SDL_Texture * HueSlide;
static SDL_Texture * SVPallet;

static void UIPickerHSV_CreateHueSlide(SDL_Renderer * rend, int height)
{
   SDL_Surface * surf;
   int i;
   surf = SDLSurfaceDraw_New(1, height);
   for(i = 0; i < height; i++)
   {
      struct colorHSV color = { (i * 360.0) / height, 1, 1, 1 };
      SDLSurfaceDraw_SetHSV(surf, 0, i, &color);
   }
   HueSlide = SDL_CreateTextureFromSurface(rend, surf);
   SDL_FreeSurface(surf);
}

static void UIPickerHSV_CreateSVPallet(SDL_Renderer * rend, double hue, int width, int height)
{
   SDL_Surface * surf;
   int y, x;
   surf = SDLSurfaceDraw_New(width, height);
   for(y = 0; y < height; y++)
   {
      for(x = 0; x < width; x++)
      {
         struct colorHSV color = { hue, 1 - (x / (double)width), 1 - (y / (double)height), 1 };
         SDLSurfaceDraw_SetHSV(surf, x, y, &color);
      }
   }
   SVPallet = SDL_CreateTextureFromSurface(rend, surf);
   SDL_FreeSurface(surf);
}

void UIPickerHSV_Init(SDL_Renderer * rend)
{
   UIPickerHSV_CreateHueSlide(rend, 128);
   UIPickerHSV_CreateSVPallet(rend, Color.h, 128, 128);
}

void UIPickerHSV_Destroy(void)
{
   SDL_DestroyTexture(HueSlide);
   SDL_DestroyTexture(SVPallet);
}

void UIPickerHSV_SetHSV(const struct colorHSV * hsv)
{
   Color.h = hsv->h;
   Color.s = hsv->s;
   Color.v = hsv->v;
   Color.a = hsv->a;
   HueDirty = true;
}
const struct colorHSV * UIPickerHSV_GetHSV(void)
{
   return &Color;
}

bool UIPickerHSV_HasColorChanged(void)
{
   bool changed = ColorChangedFlag;
   ColorChangedFlag = false;
   return changed;
}

void UIPickerHSV_SetLayout(struct uiLayoutComponent * comp)
{
   UILayout_SetSize(comp, 145, 128, 16, 16);
}

void UIPickerHSV_Render(SDL_Renderer * rend, const struct uiLayoutBox * comp)
{
   SDL_Rect rect;
   int hueOffset;
   int svx, svy;
   if(HueDirty)
   {
      UIPickerHSV_CreateSVPallet(rend, Color.h, 128, 128);
      HueDirty = false;
   }
   /*
   SDL_SetRenderDrawColor(rend, 255, 0, 0, 255);
   rect.x = 1;
   rect.y = 1;
   rect.w = 20;
   rect.h = 20;
   SDL_RenderDrawRect(rend, &rect);
   */

   rect.x = comp->computedOffset.x;
   rect.y = comp->computedOffset.y;
   rect.w = 16;
   rect.h = 128;

   
   SDL_RenderCopy(rend, HueSlide, NULL, &rect);
   SDL_SetRenderDrawColor(rend, 255, 
                                255, 
                                255, 
                                255);
   
   hueOffset = (int)((Color.h / 360.0) * rect.h);
   SDL_RenderDrawLine(rend, comp->computedOffset.x,              comp->computedOffset.y + hueOffset, 
                            comp->computedOffset.x + rect.w - 1, comp->computedOffset.y + hueOffset);
   SDL_SetRenderDrawColor(rend, 0, 
                                0, 
                                0, 
                                255);
   SDL_RenderDrawLine(rend, comp->computedOffset.x,              comp->computedOffset.y + hueOffset + 1, 
                            comp->computedOffset.x + rect.w - 1, comp->computedOffset.y + hueOffset + 1);
   
   rect.x = comp->computedOffset.x + 17;
   rect.y = comp->computedOffset.y;
   rect.w = 128;
   rect.h = 128;
   SDL_RenderCopy(rend, SVPallet, NULL, &rect);
  
   svx = (1 - Color.s) * 128;
   svy = (1 - Color.v) * 128;
  
   SDL_SetRenderDrawColor(rend, 255, 
                                255, 
                                255, 
                                255);

   SDL_RenderDrawLine(rend, rect.x + svx - 1,  rect.y + svy, 
                            rect.x + svx - 10, rect.y + svy);
   SDL_RenderDrawLine(rend, rect.x + svx,      rect.y + svy + 1, 
                            rect.x + svx,      rect.y + svy + 10);
   
   SDL_SetRenderDrawColor(rend, 0, 
                                0, 
                                0, 
                                255);
   SDL_RenderDrawLine(rend, rect.x + svx + 1,  rect.y + svy, 
                            rect.x + svx + 10, rect.y + svy);
   SDL_RenderDrawLine(rend, rect.x + svx,      rect.y + svy - 1, 
                            rect.x + svx,      rect.y + svy - 10);
                                      
     
}

inline static double Saturate(double value, double min, double max)
{
   if(value < min) return min;
   if(value > max) return max;
   return value;
}

void UIPickerHSV_InputEvent(struct kieInputEvent * event)
{
   int offsetX, offsetY;
   Input_GetCursorVector(event->type, &offsetX, &offsetY);
   if(offsetX != 0)
   {
      Color.s = Saturate(Color.s - offsetX * 0.05, 0, 1);
      ColorChangedFlag = true;
   }
   
   if(offsetY != 0)
   {
      Color.v = Saturate(Color.v - offsetY * 0.05, 0, 1);
      ColorChangedFlag = true;
   }
}

