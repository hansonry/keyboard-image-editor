#include <stddef.h>
#include "UI.h"


static inline int intMax(int a, int b)
{
   return a > b ? a : b;
}

static inline void UILayout_GetFrameDim(struct uiDimension * dims, 
                                        const struct uiDimension * comp,
                                        const struct uiSides * sides)
{
   dims->width  = comp->width  + sides->left + sides->right;
   dims->height = comp->height + sides->top  + sides->bottom;
}

static inline void UILayout_ComponentAnchorAndPaddingComputation(struct uiLayoutComponent * comp)
{
   struct uiDimension paddedBoxDim;
   struct uiOffset paddedBoxOffset;
   
   paddedBoxDim.width  = comp->frame.computedDim.width  - comp->padding.left - 
                                                          comp->padding.right;
   paddedBoxDim.height = comp->frame.computedDim.height - comp->padding.top  - 
                                                          comp->padding.bottom;
   paddedBoxOffset.x = comp->frame.computedOffset.x + comp->padding.left;
   paddedBoxOffset.y = comp->frame.computedOffset.y + comp->padding.top;
   
   // Figure out x position without padding
   if(paddedBoxDim.width > comp->component.desiredDim.width && !comp->expandX)
   {
      switch(comp->anchor)
      {
      default:
      case eUIA_TopLeft:
      case eUIA_Left:
      case eUIA_BottomLeft:
         comp->component.computedOffset.x = paddedBoxOffset.x;
         break;
      case eUIA_TopRight:
      case eUIA_Right:
      case eUIA_BottomRight:
         comp->component.computedOffset.x = paddedBoxOffset.x + 
                                            paddedBoxDim.width - 
                                            comp->component.desiredDim.width;
         break;
      case eUIA_Center:
      case eUIA_Top:
      case eUIA_Bottom:
         comp->component.computedOffset.x = paddedBoxOffset.x + 
                                            ((paddedBoxDim.width - comp->component.desiredDim.width) / 2);
         break;
      }
      comp->component.computedDim.width = comp->component.desiredDim.width;
   }
   else
   {
      comp->component.computedOffset.x = paddedBoxOffset.x;
      comp->component.computedDim.width = paddedBoxDim.width;
   }

   // Figure out vertial position without padding

   if(paddedBoxDim.height > comp->component.desiredDim.height && !comp->expandY)
   {
      switch(comp->anchor)
      {
      default:
      case eUIA_TopLeft:
      case eUIA_Top:
      case eUIA_TopRight:
         comp->component.computedOffset.y = paddedBoxOffset.y;
         break;
      case eUIA_BottomLeft:
      case eUIA_Bottom:
      case eUIA_BottomRight:
         comp->component.computedOffset.y = paddedBoxOffset.y + 
                                            paddedBoxDim.height - 
                                            comp->component.desiredDim.height;
         break;
      case eUIA_Center:
      case eUIA_Left:
      case eUIA_Right:
         comp->component.computedOffset.y = paddedBoxOffset.y + 
                                            ((paddedBoxDim.height - comp->component.desiredDim.height) / 2); 
         break;
      }
      comp->component.computedDim.height = comp->component.desiredDim.height;
   }
   else
   {
      comp->component.computedOffset.y = paddedBoxOffset.y;
      comp->component.computedDim.height = paddedBoxDim.height;
   }

}



void UILayout_Size(struct uiLayoutComponent * root)
{
   struct uiLayoutComponent * temp;
   // Size all the children
   temp = root->firstChild;
   while(temp != NULL)
   {
      UILayout_Size(temp);
      temp = temp->nextSibling;
   }
   if(root->sizeCallback != NULL)
   {
      root->sizeCallback(root);
   }
   
   // Update the frame
   UILayout_GetFrameDim(&root->frame.desiredDim, 
                        &root->component.desiredDim, 
                        &root->padding);
   UILayout_GetFrameDim(&root->frame.minDim, 
                        &root->component.minDim, 
                        &root->padding);
}

static inline void UILayout_SetChildrenFrameSize(struct uiLayoutComponent * parent)
{
   struct uiLayoutComponent * c;
   c = parent->firstChild;
   while(c != NULL)
   {
      c->frame.computedDim.width  = intMax(parent->component.computedDim.width,  c->frame.minDim.width);
      c->frame.computedDim.height = intMax(parent->component.computedDim.height, c->frame.minDim.height);
      c->frame.computedOffset.x   = parent->component.computedOffset.x;
      c->frame.computedOffset.y   = parent->component.computedOffset.y;
      c = c->nextSibling;
   }
   
}

static void UILayout_LayoutAlignRecursivly(struct uiLayoutComponent * comp)
{
   struct uiLayoutComponent * temp;
   
   UILayout_ComponentAnchorAndPaddingComputation(comp);
   
   if(comp->layoutCallback != NULL)
   {
      comp->layoutCallback(comp);
   }
   else
   {
      UILayout_SetChildrenFrameSize(comp);
   }

   temp = comp->firstChild;
   while(temp != NULL)
   {
      UILayout_LayoutAlignRecursivly(temp);
      temp = temp->nextSibling;
   }

}



void UILayout_Layout(struct uiLayoutComponent * root,
                     int x, int y,
                     int fitWidth, int fitHeight, 
                     struct uiDimension * computedSize)
{
   
   root->frame.computedOffset.x = x;
   root->frame.computedOffset.y = y;
   
   root->frame.computedDim.width  = intMax(fitWidth,  root->frame.minDim.width);
   root->frame.computedDim.height = intMax(fitHeight, root->frame.minDim.height);

   UILayout_LayoutAlignRecursivly(root);

   if(computedSize != NULL)
   {
      computedSize->width  = root->frame.computedDim.width;
      computedSize->height = root->frame.computedDim.height;
   }

}

void UILayout_SizeAndLayout(struct uiLayoutComponent * root,
                            int x, int y,
                            int fitWidth, int fitHeight, 
                            struct uiDimension * computedSize)
{
   UILayout_Size(root);
   UILayout_Layout(root, x, y, fitWidth, fitHeight, computedSize);
}

static inline void UILayout_InitBox(struct uiLayoutBox * box)
{
   box->minDim.width       = 0;
   box->minDim.height      = 0;
   box->desiredDim.width   = 0;
   box->desiredDim.height  = 0;
   box->computedDim.width  = 0;
   box->computedDim.height = 0;
   box->computedOffset.x   = 0;
   box->computedOffset.y   = 0;
}

void UILayout_InitComponent(struct uiLayoutComponent * comp)
{
   comp->parent         = NULL;
   comp->firstChild     = NULL;
   comp->nextSibling    = NULL;
   comp->padding.left   = 0;
   comp->padding.top    = 0;
   comp->padding.right  = 0;
   comp->padding.bottom = 0;
   comp->anchor         = eUIA_TopLeft;
   comp->layoutCallback = NULL;
   comp->sizeCallback   = NULL;
   comp->expandX        = 0;
   comp->expandY        = 0;
   UILayout_InitBox(&comp->component);
   UILayout_InitBox(&comp->frame);

}

void UILayout_SetSize(struct uiLayoutComponent * comp,
                      int desiredWidth, int desiredHeight,
                      int minWidth,     int minHeight)
{
   comp->component.minDim.width      = minWidth;
   comp->component.minDim.height     = minHeight;
   comp->component.desiredDim.width  = desiredWidth;
   comp->component.desiredDim.height = desiredHeight;
}


void UILayout_SetPadding(struct uiLayoutComponent * comp, 
                         int left, int top, int right, int bottom)
{
   comp->padding.left   = left;
   comp->padding.top    = top;
   comp->padding.right  = right;
   comp->padding.bottom = bottom;
}

void UILayout_SetLayoutFunction(struct uiLayoutComponent * comp, 
                                uiLayoutLayoutFunction layoutCallback)
{
   comp->layoutCallback = layoutCallback;
}

void UILayout_SetSizeFunction(struct uiLayoutComponent * comp, 
                              uiLayoutSizeFunction sizeCallback)
{
   comp->sizeCallback = sizeCallback;
}

void UILayout_AddChild(struct uiLayoutComponent * parent, 
                       struct uiLayoutComponent * child)
{
   struct uiLayoutComponent * temp;

   child->parent = parent;
   if(parent->firstChild == NULL)
   {
      parent->firstChild = child;
   }
   else
   {
      temp = parent->firstChild;
      while(temp->nextSibling != NULL)
      {
         temp = temp->nextSibling;
      }
      temp->nextSibling = child;
   }
}

static inline void UILayout_DimensionCopy(struct uiDimension * dest, 
                                          const struct uiDimension * src)
{
   dest->width  = src->width;
   dest->height = src->height;
}

static void UILayout_FrameMinAndSum(const struct  uiLayoutComponent * comp, 
                                    struct uiDimension * minDesired,
                                    struct uiDimension * sumDesired,
                                    struct uiDimension * minMin,
                                    struct uiDimension * sumMin,
                                    int * expandX, int * expandY)
{
   struct uiLayoutComponent * c;
   struct uiDimension lMinDesired = { 0, 0 };
   struct uiDimension lSumDesired = { 0, 0 };
   struct uiDimension lMinMin     = { 0, 0 };
   struct uiDimension lSumMin     = { 0, 0 };
   int lExpandX = 0;
   int lExpandY = 0;
   
   c = comp->firstChild;
   while(c != NULL)
   {
      struct uiLayoutBox * frame = &c->frame;
      if(lMinDesired.width < frame->desiredDim.width)
      {
         lMinDesired.width = frame->desiredDim.width;
      }
      if(lMinDesired.height < frame->desiredDim.height)
      {
         lMinDesired.height = frame->desiredDim.height;
      }
      if(lMinMin.width < frame->minDim.width)
      {
         lMinMin.width = frame->minDim.width;
      }
      if(lMinMin.height < frame->minDim.height)
      {
         lMinMin.height = frame->minDim.height;
      }

      if(c->expandX) lExpandX = 1;
      if(c->expandY) lExpandY = 1;

      lSumDesired.width  += frame->desiredDim.width;
      lSumDesired.height += frame->desiredDim.height;
      lSumMin.width      += frame->minDim.width; 
      lSumMin.height     += frame->minDim.height; 
      c = c->nextSibling;
   }

   if(minDesired != NULL)
   {
      UILayout_DimensionCopy(minDesired, &lMinDesired);
   }
   if(sumDesired != NULL)
   {
      UILayout_DimensionCopy(sumDesired, &lSumDesired);
   }
   if(minMin != NULL)
   {
      UILayout_DimensionCopy(minMin, &lMinMin);
   }
   if(minMin != NULL)
   {
      UILayout_DimensionCopy(sumMin, &lSumMin);
   }
   if(expandX != NULL)
   {
      (*expandX) = lExpandX;
   }
   if(expandY != NULL)
   {
      (*expandY) = lExpandY;
   }
}

/*
static void UILayout_SumOfExtend(const struct  uiLayoutComponent * comp, 
                                 struct uiDimension * sum,
                                 struct uiDimension * sumNoPadding,
                                 int * expandXCount, int * expandYCount)
{
   struct uiLayoutComponent * c;
   struct uiDimension lSum = { 0, 0 };
   struct uiDimension lSumNoPadding = { 0, 0 };
   int lExpandXCount = 0;
   int lExpandYCount = 0;
   
   c = comp->firstChild;
   while(c != NULL)
   {
      if(c->expandX || c->expandY)
      {
         struct uiDimension desired;
         UILayout_GetFrameDim(&desired, &c->desiredDim, &c->padding);
        
         if(c->expandX)
         {
            lSum.width += c->frame.desiredDim.width;
            lSumNoPadding.width += c->component.desiredDim.width;
            lExpandXCount ++;
         }
         if(c->expandY)
         {
            lSum.height += c->frame.desiredDim.height;
            lSumNoPadding.height += c->component.desiredDim.height;
            lExpandYCount ++;
         }
      }

      c = c->nextSibling;
   }

   if(sum != NULL)
   {
      UILayout_DimensionCopy(sum, &lSum);
   }

   if(sumNoPadding != NULL)
   {
      UILayout_DimensionCopy(sumNoPadding, &lSumNoPadding);
   }

   if(expandXCount != NULL)
   {
      (*expandXCount) = lExpandXCount;
   }

   if(expandYCount != NULL)
   {
      (*expandYCount) = lExpandYCount;
   }
}
*/

static void UILayout_VSplitLayout(struct uiLayoutComponent * comp)
{
   // New Code starts here
   struct uiLayoutComponent * c;   
   int yOffset;
   int expandHeight;
   int expandYCount;
   int extraHeight;

   // Compute expand details
   expandYCount = 0;
   extraHeight = comp->component.computedDim.height;
   expandHeight = 0;
   c = comp->firstChild;
   while(c != NULL)
   {
      if(c->expandY)
      {
         expandYCount ++;
         expandHeight += c->frame.desiredDim.height;
      }
      else
      {
         extraHeight -= c->frame.desiredDim.height;
      }
      
      c = c->nextSibling;
   }
   
   UILayout_SetChildrenFrameSize(comp);

   // Figure out how we are going to squish the heights
   if(comp->component.computedDim.height <= comp->component.minDim.height)
   {
      // Alloted space is less than minimum. Set all heights to min.
      c = comp->firstChild;
      while(c != NULL)
      {
         c->frame.computedDim.height = c->frame.minDim.height;
         
         c = c->nextSibling;
      }
   }
   else if(comp->component.computedDim.height <= comp->component.desiredDim.height || 
           expandYCount == 0)
   {
      // Set all hights realitive to desired heights
      int parentDesiredFlex = comp->component.desiredDim.height - comp->component.minDim.height;
      int parentActualFlex  = comp->component.computedDim.height - comp->component.minDim.height;
      double parentFlexPercent = parentActualFlex / (double)parentDesiredFlex;
      c = comp->firstChild;
      while(c != NULL)
      {
         int childFlex = c->frame.desiredDim.height - c->frame.minDim.height;
         c->frame.computedDim.height = c->frame.minDim.height + 
                                       (int)(childFlex * parentFlexPercent);
         
         c = c->nextSibling;
      }
   }
   else
   {
      // Set all non expand heights to desired heights and size expand components
      c = comp->firstChild;
      while(c != NULL)
      {
         if(c->expandY)
         {
            double percent;
            if(expandHeight == 0)
            {
               percent = 1 / (double)expandYCount;
            }
            else
            {
               percent = c->frame.desiredDim.height / (double) expandHeight;
            }
            c->frame.computedDim.height = (int)(percent * extraHeight);            
         }
         else
         {
            c->frame.computedDim.height = c->frame.desiredDim.height;
         }
         c = c->nextSibling;
      }
      
   }
   
   // Set Y positions based off computed Height
   yOffset = comp->component.computedOffset.y;
   c = comp->firstChild;
   while(c != NULL)
   {
      c->frame.computedOffset.y = yOffset;
      
      yOffset += c->frame.computedDim.height;
      c = c->nextSibling;
   }

}

static void UILayout_VSplitSize(struct uiLayoutComponent * comp)
{
   struct uiDimension minDesired;
   struct uiDimension sumDesired;
   struct uiDimension minMin;
   struct uiDimension sumMin;

   UILayout_FrameMinAndSum(comp, &minDesired,    &sumDesired,
                            &minMin,        &sumMin,
                            &comp->expandX, &comp->expandY);

   comp->component.desiredDim.width  = minDesired.width;
   comp->component.desiredDim.height = sumDesired.height;
   comp->component.minDim.width      = minMin.width;
   comp->component.minDim.height     = sumMin.height;
}

void UILayout_InitVSplit(struct uiLayoutComponent * comp)
{
   UILayout_InitComponent(comp);
   comp->layoutCallback = UILayout_VSplitLayout;
   comp->sizeCallback   = UILayout_VSplitSize;
}


static void UILayout_HSplitLayout(struct uiLayoutComponent * comp)
{
   // New Code starts here
   struct uiLayoutComponent * c;   
   int xOffset;
   int expandWidth;
   int expandXCount;
   int extraWidth;

   // Compute expand details
   expandXCount = 0;
   extraWidth = comp->component.computedDim.width;
   expandWidth = 0;
   c = comp->firstChild;
   while(c != NULL)
   {
      if(c->expandX)
      {
         expandXCount ++;
         expandWidth += c->frame.desiredDim.width;
      }
      else
      {
         extraWidth -= c->frame.desiredDim.width;
      }
      
      c = c->nextSibling;
   }
   
   UILayout_SetChildrenFrameSize(comp);

   // Figure out how we are going to squish the heights
   if(comp->component.computedDim.width <= comp->component.minDim.width)
   {
      // Alloted space is less than minimum. Set all heights to min.
      c = comp->firstChild;
      while(c != NULL)
      {
         c->frame.computedDim.width = c->frame.minDim.width;
         
         c = c->nextSibling;
      }
   }
   else if(comp->component.computedDim.width <= comp->component.desiredDim.width || 
           expandXCount == 0)
   {
      // Set all widths realitive to desired heights
      int parentDesiredFlex = comp->component.desiredDim.width - comp->component.minDim.width;
      int parentActualFlex  = comp->component.computedDim.width - comp->component.minDim.width;
      double parentFlexPercent = parentActualFlex / (double)parentDesiredFlex;
      c = comp->firstChild;
      while(c != NULL)
      {
         int childFlex = c->frame.desiredDim.width - c->frame.minDim.width;
         c->frame.computedDim.width = c->frame.minDim.width + 
                                       (int)(childFlex * parentFlexPercent);
         
         c = c->nextSibling;
      }
   }
   else
   {
      // Set all non expand heights to desired heights and size expand components
      c = comp->firstChild;
      while(c != NULL)
      {
         if(c->expandX)
         {
            double percent;
            if(expandWidth == 0)
            {
               percent = 1 / (double)expandXCount;
            }
            else
            {
               percent = c->frame.desiredDim.width / (double) expandWidth;
            }
            c->frame.computedDim.width = (int)(percent * extraWidth);            
         }
         else
         {
            c->frame.computedDim.width = c->frame.desiredDim.width;
         }
         c = c->nextSibling;
      }
      
   }
   
   // Set Y positions based off computed Height
   xOffset = comp->component.computedOffset.x;
   c = comp->firstChild;
   while(c != NULL)
   {
      c->frame.computedOffset.x = xOffset;
      
      xOffset += c->frame.computedDim.width;
      c = c->nextSibling;
   }

}


static void UILayout_HSplitSize(struct uiLayoutComponent * comp)
{
   struct uiDimension minDesired;
   struct uiDimension sumDesired;
   struct uiDimension minMin;
   struct uiDimension sumMin;

   UILayout_FrameMinAndSum(comp, &minDesired,    &sumDesired,
                            &minMin,        &sumMin,
                            &comp->expandX, &comp->expandY);


   comp->component.desiredDim.width  = sumDesired.width;
   comp->component.desiredDim.height = minDesired.height;
   comp->component.minDim.width      = sumMin.width;
   comp->component.minDim.height     = minMin.height;
}

void UILayout_InitHSplit(struct uiLayoutComponent * comp)
{
   UILayout_InitComponent(comp);
   comp->layoutCallback = UILayout_HSplitLayout;
   comp->sizeCallback   = UILayout_HSplitSize;
}
