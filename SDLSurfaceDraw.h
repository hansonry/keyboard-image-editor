#ifndef __SDLSURFACEDRAW_H__
#define __SDLSURFACEDRAW_H__
#include "SDL.h"
#include "Color.h"

SDL_Surface * SDLSurfaceDraw_New(int width, int height);

void SDLSurfaceDraw_SetRGB(SDL_Surface * surf, int x, int y, const struct colorRGB * color);
void SDLSurfaceDraw_SetHSV(SDL_Surface * surf, int x, int y, const struct colorHSV * color);
void SDLSurfaceDraw_GetRGB(SDL_Surface * surf, int x, int y, struct colorRGB * color);


#endif // __SDLSURFACEDRAW_H__
