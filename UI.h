#ifndef __UI_H__
#define __UI_H__

struct uiOffset
{
   int x;
   int y;
};

struct uiDimension
{
   int width;
   int height;
};

struct uiSides
{
   int left;
   int top;
   int right;
   int bottom;
};

enum uiAnchor
{
   eUIA_Center,
   eUIA_TopLeft,
   eUIA_Top,
   eUIA_TopRight,
   eUIA_Right,
   eUIA_BottomRight,
   eUIA_Bottom,
   eUIA_BottomLeft,
   eUIA_Left
};

struct uiLayoutComponent;

typedef void (*uiLayoutLayoutFunction)(struct uiLayoutComponent * comp);
typedef void (*uiLayoutSizeFunction)(struct uiLayoutComponent * comp);

struct uiLayoutBox
{
   // Inputs
   struct uiDimension minDim;
   struct uiDimension desiredDim;

   // Outputs
   struct uiDimension computedDim;
   struct uiOffset computedOffset;    
};

struct uiLayoutComponent
{
   struct uiLayoutBox component;
   struct uiLayoutBox frame;
      
   // Inputs
   enum uiAnchor      anchor;
   struct uiSides     padding;
   struct uiLayoutComponent * parent;
   struct uiLayoutComponent * nextSibling;
   struct uiLayoutComponent * firstChild;
   uiLayoutLayoutFunction layoutCallback;
   uiLayoutSizeFunction sizeCallback;
   int expandX;
   int expandY;
};

void UILayout_Size(struct uiLayoutComponent * root);
void UILayout_Layout(struct uiLayoutComponent * root,
                     int x, int y,
                     int fitWidth, int fitHeight, 
                     struct uiDimension * computedSize);

void UILayout_SizeAndLayout(struct uiLayoutComponent * root,
                            int x, int y,
                            int fitWidth, int fitHeight, 
                            struct uiDimension * computedSize);
                     
void UILayout_InitComponent(struct uiLayoutComponent * comp);

void UILayout_SetSize(struct uiLayoutComponent * comp,
                      int desiredWidth, int desiredHeight,
                      int minWidth,     int minHeight);

void UILayout_SetPadding(struct uiLayoutComponent * comp, 
                         int left, int top, int right, int bottom);

void UILayout_SetLayoutFunction(struct uiLayoutComponent * comp, 
                                uiLayoutLayoutFunction layoutCallback);

void UILayout_SetSizeFunction(struct uiLayoutComponent * comp, 
                              uiLayoutSizeFunction sizeCallback);
                     
void UILayout_AddChild(struct uiLayoutComponent * parent, 
                       struct uiLayoutComponent * child);


// Special Component Functions

void UILayout_InitVSplit(struct uiLayoutComponent * comp);

void UILayout_InitHSplit(struct uiLayoutComponent * comp);



#endif // __UI_H__
