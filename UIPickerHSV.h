#ifndef __UIPickerHSV_H__
#define __UIPickerHSV_H__
#include <stdbool.h>
#include "Color.h"
#include "SDL.h"
#include "UI.h"
#include "Input.h"

void UIPickerHSV_Init(SDL_Renderer * rend);
void UIPickerHSV_Destroy(void);

void UIPickerHSV_SetHSV(const struct colorHSV * hsv);
const struct colorHSV * UIPickerHSV_GetHSV(void);
bool UIPickerHSV_HasColorChanged(void);

void UIPickerHSV_SetLayout(struct uiLayoutComponent * comp);

void UIPickerHSV_Render(SDL_Renderer * rend, const struct uiLayoutBox * comp);

void UIPickerHSV_InputEvent(struct kieInputEvent * event);

#endif // __UIPickerHSV_H__
