#ifndef __COMMANDBUFFER_H__
#define __COMMANDBUFFER_H__

void CommandBuffer_Init(TTF_Font * font);

void CommandBuffer_Destroy(void);


void CommandBuffer_SetText(const char * text);

void CommandBuffer_AddToHistroy(void);

int CommandBuffer_MoveCursorLeft(void);

int CommandBuffer_MoveCursorRight(void);

void CommandBuffer_SetCursorPos(size_t pos);

void CommandBuffer_SetCursorPosToEnd(void);

void CommandBuffer_AddTextAtCursor(const char * text);

int CommandBuffer_Backspace(void);

int CommandBuffer_Delete(void);

size_t CommandBuffer_GetCount(void);

const char * CommandBuffer_GetText(void);

size_t CommandBuffer_GetCursorPos(void);


// SDL Specific Stuff

void CommandBuffer_SetFont(TTF_Font * font);

void CommandBuffer_SetColor(Uint8 r, Uint8 g, Uint8 b, Uint8 a);


SDL_Texture * CommandBuffer_GetTexture(SDL_Renderer * rend, int * width, int * height);

void CommandBuffer_StartTextInput(void);

void CommandBuffer_StopTextInput(void);

int CommandBuffer_IsTextInputActive(void);

void CommandBuffer_HandelSDLEvent(SDL_Event * event);

int CommandBuffer_GetOffsetOfCharacter(size_t index);

void CommandBuffer_GetSizeOfText(int * width, int * height);

void CommandBuffer_RenderText(SDL_Renderer * rend, int x, int y);
void CommandBuffer_RenderTextAtHeight(SDL_Renderer * rend, int x, int y, int height);



#endif // __COMMANDBUFFER_H__
