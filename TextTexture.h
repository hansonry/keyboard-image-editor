#ifndef __TEXTTEXTURE_H__
#define __TEXTTEXTURE_H__

struct textTexture
{
   TTF_Font * font;
   SDL_Color color;
   SDL_Texture * texture;
   int textureWidth;
   int textureHeight;
   int textWidth;
   int textHeight;
   char * text;
   size_t textSize;
   bool textureIsOutOfDate;
   bool textSizeIsOutOfDate;
};

void TextTexture_Init(struct textTexture * tt, TTF_Font * font);

void TextTexture_Destroy(struct textTexture * tt);

void TextTexture_MakeRoomFor(struct textTexture * tt, size_t size);

void TextTexture_Write(struct textTexture * tt, const char * format, ...);

void TextTexture_VWrite(struct textTexture * tt, const char * format, va_list args);



void TextTexture_GetSize(struct textTexture * tt, int * width, int * height);

SDL_Texture * TextTexture_GetTexture(struct textTexture * tt, SDL_Renderer * rend, int * width, int * height);

void TextTexture_Render(struct textTexture * tt, SDL_Renderer * rend, int x, int y);


#endif // __TEXTTEXTURE_H__
