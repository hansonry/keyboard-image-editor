#include <stddef.h>
#include "Input.h"

void Input_GetCursorVector(enum kieInputEventType type, int * x, int * y)
{
   int lx, ly;
   
   switch(type)
   {
   case eKIET_CursorUp:
      lx = 0;
      ly = -1;
      break;
   case eKIET_CursorUpLeft:
      lx = -1;
      ly = -1;
      break;
   case eKIET_CursorLeft:
      lx = -1;
      ly = 0;
      break;
   case eKIET_CursorDownLeft:
      lx = -1;
      ly = 1;
      break;
   case eKIET_CursorDown:
      lx = 0;
      ly = 1;
      break;
   case eKIET_CursorDownRight:
      lx = 1;
      ly = 1;
      break;
   case eKIET_CursorRight:
      lx = 1;
      ly = 0;
      break;
   case eKIET_CursorUpRight:
      lx = 1;
      ly = -1;
      break;
   default:
      lx = 0;
      ly = 0;
      break;
   }
   
   if(x != NULL)
   {
      *x = lx;
   }
   
   if(y != NULL)
   {
      *y = ly;
   }
}